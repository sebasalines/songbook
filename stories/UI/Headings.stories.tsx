import * as React from 'react';
import { storiesOf } from '@storybook/react';
import Heading from '../../src/components/common/Heading';

storiesOf('Heading', module)
  .add(
    '<Heading />',
    () => (
      <Heading as="h1" value="Sample Heading" />
    )
  );