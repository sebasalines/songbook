import * as React from 'react';
import * as _ from 'lodash';
import uuid from 'uuid/v4';
import MIDISounds from 'midi-sounds-react';
import * as actions from '../constants/midi';

export interface MidiReducer {
  queue: any[];
};
const initialState = {
  queue: [],
};

const midi = (state: MidiReducer = initialState, action): MidiReducer => {
  const { type, data } = action;
  switch (type) {
    case actions.SET_PLAYER_INSTANCE:
      return {
        ...state,
      };
    case actions.PLAY_NOTE:
      return {
        ...state,
        queue: [
          ...state.queue,
          {
            uuid: uuid(),
            type: 'single',
            pitch: data.pitch,
          },
        ],
      };
    case actions.STRUM_CHORD:
      return {
        ...state,
        queue: [
          ...state.queue,
          {
            uuid: uuid(),
            type: 'strum',
            up: data.up,
            pitches: _.compact(data.pitches),
          },
        ],
      };
    default:
      return state;
  }
};
export default midi;
