import {
	CREATE_SONG,
	CREATE_SONG_SUCCESS,
	CREATE_SONG_FAILED,
	UPDATE_SONG,
	UPDATE_SONG_SUCCESS,
	UPDATE_SONG_FAILED,
	DELETE_SONG,
	DELETE_SONG_SUCCESS,
	DELETE_SONG_FAILED,
	GET_SONG,
	GET_SONG_SUCCESS,
	GET_SONG_FAILED,
} from '../constants';

const initialState = {
	isLoading: false,
	songs: []
}

const songReducer = (state: any = initialState, action: { type: string, data: any}) => {
	const { type, data } = action
	switch (type) {
		case CREATE_SONG:
		case UPDATE_SONG:
		case DELETE_SONG:
			return {
				...state,
				isLoading: true
			}
		case CREATE_SONG_FAILED:
		case UPDATE_SONG_FAILED:
		case DELETE_SONG_FAILED:
			return {
				...state,
				isLoading: false
			}
		case CREATE_SONG_SUCCESS:
			return {
				...state,
				isLoading: false,
				songs: [...state.songs, data.song]
			}
		case UPDATE_SONG_SUCCESS:
			return {
				...state,
				isLoading: false,
				songs: [...state.songs.map(item => {
					return item.id === data.song.id ?
					{...item,...data.song} : item
				})]
			}
		case DELETE_SONG_SUCCESS:
			return {
				...state,
				isLoading: false,
				songs: [...state.songs.filter(item => item.id !== data.song.id)]
			}					
		default:
			return state; 
	}
}

export default songReducer;