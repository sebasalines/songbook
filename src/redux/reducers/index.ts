import { combineReducers } from 'redux';
import chordReducer from './chords';
import progressionReducer from './progressions';
import sectionReducer from './sections';
import songReducer from './songs';
import midi from './midi';

const appState: any = combineReducers({
  chordReducer,
  progressionReducer,
  sectionReducer,
  songReducer,
  midi,
});
export default appState;
