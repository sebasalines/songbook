import {
	CREATE_SECTION,
	CREATE_SECTION_SUCCESS,
	CREATE_SECTION_FAILED,
	UPDATE_SECTION,
	UPDATE_SECTION_SUCCESS,
	UPDATE_SECTION_FAILED,
	DELETE_SECTION,
	DELETE_SECTION_SUCCESS,
	DELETE_SECTION_FAILED,
	GET_SECTION,
	GET_SECTION_SUCCESS,
	GET_SECTION_FAILED,
} from '../constants';

const initialState = {
	isLoading: false,
	sections: []
}

const sectionReducer = (state: any = initialState, action: { type: string, data:any }) => {
	const { type, data } = action
	switch(type){
		case CREATE_SECTION:
		case UPDATE_SECTION:
		case DELETE_SECTION:
			return {
				...state,
				isLoading: true
			}
		case CREATE_SECTION_FAILED:
		case UPDATE_SECTION_FAILED:
		case DELETE_SECTION_FAILED:
			return {
				...state,
				isLoading: false
			}
		case CREATE_SECTION_SUCCESS:
			return {
				...state,
				isLoading: false,
				sections: [...state.sections, data.section]
			}
		case UPDATE_SECTION_SUCCESS:
			return {
				...state,
				isLoading: false,
				sections: [...state.sections.map(item => {
					return item.id === data.section.id ?
					{...item,...data.section }: item
				})]
			}
		case DELETE_SECTION_SUCCESS:
			return {
				...state,
				isLoading: false,
				sections: [...state.sections.filter(item => item.id !== data.section.id)]
			}
		default:
			return state;
	}
}

export default sectionReducer;