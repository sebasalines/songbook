import {
	CREATE_CHORD,
	CREATE_CHORD_SUCCESS,
	CREATE_CHORD_FAILED,
	UPDATE_CHORD,
	UPDATE_CHORD_SUCCESS,
	UPDATE_CHORD_FAILED,
	DELETE_CHORD,
	DELETE_CHORD_SUCCESS,
	DELETE_CHORD_FAILED,
	GET_CHORD,
	GET_CHORD_SUCCESS,
	GET_CHORD_FAILED,
} from '../constants';


const initialState = {
	isLoading: false,
	chords: []
}

const chordReducer = (state: any = initialState, action: { type: string, data: any }) => {
  const { type, data } = action;
  switch (type) {
  	case CREATE_CHORD:
  	case UPDATE_CHORD:
  	case DELETE_CHORD:
    case GET_CHORD:
  		return {
  			...state,
  			isLoading: true
  		};
  	case CREATE_CHORD_FAILED:
  	case UPDATE_CHORD_FAILED:
  	case DELETE_CHORD_FAILED:
    case GET_CHORD_FAILED:
  		return {
  			...state,
  			isLoading: false
  		}
    case GET_CHORD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        chords: [...data.chords]
      }
  	case CREATE_CHORD_SUCCESS:
  		return {
  			...state,
  			isLoading: false,
  			chords: [...state.chords, data.chord]
  		}
  	case UPDATE_CHORD_SUCCESS:
  		return {
  			...state,
  			isLoading: false,
  			chords: [...state.chords.map( item => {
  				return item.id === data.chord.id ? {...item,...data.chord} : item;
  			})]
  		}
  	case DELETE_CHORD_SUCCESS:
  		return {
  			...state,
  			isLoading: false,
  			chords: [...state.chords.filter(item => item.id !== data.chord.id)]
  		}
    default:
      return state;
  }
}
export default chordReducer;