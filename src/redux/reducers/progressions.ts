import {
	CREATE_PROGRESSION,
	CREATE_PROGRESSION_SUCCESS,
	CREATE_PROGRESSION_FAILED,
	UPDATE_PROGRESSION,
	UPDATE_PROGRESSION_SUCCESS,
	UPDATE_PROGRESSION_FAILED,
	DELETE_PROGRESSION,
	DELETE_PROGRESSION_SUCCESS,
	DELETE_PROGRESSION_FAILED,
	GET_PROGRESSION,
	GET_PROGRESSION_SUCCESS,
	GET_PROGRESSION_FAILED,
} from '../constants';

const initialState = {
	isLoading: false,
	progressions: []
}

const progressionReducer = (state: any = initialState, action: {type: string, data: any}) => {
	const { type, data } = action;
	switch(type){
		case CREATE_PROGRESSION:
		case UPDATE_PROGRESSION:
		case DELETE_PROGRESSION:
			return {
				...state,
				isLoading: true
			}
		case CREATE_PROGRESSION_FAILED:
		case UPDATE_PROGRESSION_FAILED:
		case DELETE_PROGRESSION_FAILED:
			return {
				...state,
				isLoading: false
			}
		case CREATE_PROGRESSION_SUCCESS:
			return {
				...state,
				isLoading: false,
				progressions: [...state.progressions, data.progression]
			}
		case UPDATE_PROGRESSION_SUCCESS:
			return {
				...state,
				isLoading: false,
				progressions: [...state.progressions.map(item => {
							return	item.id === data.progression.id ? 
							{...item,...data.progression} : item
				})]
			}
		case DELETE_PROGRESSION_SUCCESS:
			return {
				...state,
				isLoading: false,
				progressions: [...state.progressions.filter(item => item.id !== data.progression.id)]
			}
		default:
			return state;
	}
}

export default progressionReducer;