import * as _ from 'lodash';
import MIDISounds from 'midi-sounds-react';
import * as actions from '../constants/midi';
import { AppState } from '../../models/redux';
import { ActiveNoteInfo } from '../../components/Chords/ChordEditor';

export const setPlayer = (player: any) => ({
  type: actions.SET_PLAYER_INSTANCE,
  data: { player },
});

export const playNote = (pitch: number | null) => ({
  type: actions.PLAY_NOTE,
  data: { pitch },
})

export const strumNotes = (notes: ActiveNoteInfo[], up = false) => {
  const pitches = notes.map(itm => itm.note ? itm.note.pitch.midi : null);
  return {
    type: actions.STRUM_CHORD,
    data: { pitches, up },
  };
};

export const removeFromQueue = (uuid: string) => ({
  type: actions.REMOVE_FROM_QUEUE,
  data: { uuid },
});