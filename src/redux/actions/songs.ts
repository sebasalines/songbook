import {
	CREATE_SONG,
	CREATE_SONG_SUCCESS,
	CREATE_SONG_FAILED,
	UPDATE_SONG,
	UPDATE_SONG_SUCCESS,
	UPDATE_SONG_FAILED,
	DELETE_SONG,
	DELETE_SONG_SUCCESS,
	DELETE_SONG_FAILED,
	GET_SONG,
	GET_SONG_SUCCESS,
	GET_SONG_FAILED,
} from '../constants';

const getSongs = () => dispatch => {
	dispatch({ type: GET_SONG });
	dispatch({ type: GET_SONG_SUCCESS });
}

const createSong= (song) => {
	return { type: CREATE_SONG_SUCCESS, data: {song} };
}

const deleteSong= (song) => {
	return { type: DELETE_SONG_SUCCESS, data: {song}};
}

const updateSong= (song) => {
	return { type: UPDATE_SONG_SUCCESS, data: {song}};
}

export const SongActions = {
	getSongs,
	createSong,
	deleteSong,
	updateSong
}