import {
	CREATE_SECTION,
	CREATE_SECTION_SUCCESS,
	CREATE_SECTION_FAILED,
	UPDATE_SECTION,
	UPDATE_SECTION_SUCCESS,
	UPDATE_SECTION_FAILED,
	DELETE_SECTION,
	DELETE_SECTION_SUCCESS,
	DELETE_SECTION_FAILED,
	GET_SECTION,
	GET_SECTION_SUCCESS,
	GET_SECTION_FAILED,
} from '../constants';

const getSections = () => dispatch => {
	dispatch({ type: GET_SECTION });
	dispatch({ type: GET_SECTION_SUCCESS });
}

const createSection= (section) => {
	return { type: CREATE_SECTION_SUCCESS, data: {section}};
}

const deleteSection= (section) => {
	return { type: DELETE_SECTION_SUCCESS, data: {section}};
}

const updateSection= (section) => {
	return { type: UPDATE_SECTION_SUCCESS, data: {section}};
}

export const SectionActions = {
	getSections,
	createSection,
	deleteSection,
	updateSection
}