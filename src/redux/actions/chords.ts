import {
	CREATE_CHORD,
	CREATE_CHORD_SUCCESS,
	CREATE_CHORD_FAILED,
	UPDATE_CHORD,
	UPDATE_CHORD_SUCCESS,
	UPDATE_CHORD_FAILED,
	DELETE_CHORD,
	DELETE_CHORD_SUCCESS,
	DELETE_CHORD_FAILED,
	GET_CHORD,
	GET_CHORD_SUCCESS,
	GET_CHORD_FAILED,
} from '../constants';

export const getChords = () => dispatch => {
	dispatch({ type: GET_CHORD });
	dispatch({ type: GET_CHORD_SUCCESS });
}

export const createChord = (chord) => {
	return { type: CREATE_CHORD_SUCCESS, data: {chord}};
}

export const deleteChord = (chord) => {
	return { type: DELETE_CHORD_SUCCESS, data: {chord}};
}

export const updateChord = (chord) => {
	return { type: UPDATE_CHORD_SUCCESS, data: {chord}};
}

export const ChordActions = {
	getChords,
	createChord,
	deleteChord,
	updateChord
}