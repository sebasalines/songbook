import {
	LOGOUT,
	GET_USER_INFO,
	GET_USER_INFO_SUCCESS,
	GET_USER_INFO_FAILED
} from '../constants';

const getUserInfo = () => dispatch => {
	dispatch({ type: GET_USER_INFO });
	dispatch({ type: GET_USER_INFO_SUCCESS });
}

const logout = () => {
	return { type: LOGOUT }
}

export const AuthActions = {
	getUserInfo,
	logout
}