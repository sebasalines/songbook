import {
	CREATE_PROGRESSION,
	CREATE_PROGRESSION_SUCCESS,
	CREATE_PROGRESSION_FAILED,
	UPDATE_PROGRESSION,
	UPDATE_PROGRESSION_SUCCESS,
	UPDATE_PROGRESSION_FAILED,
	DELETE_PROGRESSION,
	DELETE_PROGRESSION_SUCCESS,
	DELETE_PROGRESSION_FAILED,
	GET_PROGRESSION,
	GET_PROGRESSION_SUCCESS,
	GET_PROGRESSION_FAILED,
} from '../constants';

const getProgressions = () => dispatch => {
	dispatch({ type: GET_PROGRESSION });
	dispatch({ type: GET_PROGRESSION_SUCCESS });
}

const createProgression = (progression) => {
	return { type: CREATE_PROGRESSION_SUCCESS, data: {progression}};
}

const deleteProgression = (progression) => {
	return { type: DELETE_PROGRESSION_SUCCESS, data: {progression}};
}

const updateProgression = (progression) => {
	return { type: UPDATE_PROGRESSION_SUCCESS, data: {progression}};
}

export const ProgressionActions = {
	getProgressions,
	createProgression,
	deleteProgression,
	updateProgression
}