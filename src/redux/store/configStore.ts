import { compose, createStore, applyMiddleware } from 'redux';
import appState from '../reducers';
import { persistReducer, PersistConfig } from 'redux-persist';
import storage from 'redux-persist/lib/storage'

const persistConfig: PersistConfig = {
  key: 'root',
  blacklist: ['midi'],
  storage,
}

const persistedReducer = persistReducer(persistConfig, appState);
const configureStore = createStore(
  persistedReducer,
  // @ts-ignore
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);


export default configureStore;
