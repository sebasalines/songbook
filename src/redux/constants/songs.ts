export const CREATE_SONG = 'songs/CREATE_SONG';
export const CREATE_SONG_SUCCESS = 'songs/CREATE_SONG_SUCCESS';
export const CREATE_SONG_FAILED = 'songs/CREATE_SONG_FAILED';

export const UPDATE_SONG = 'songs/UPDATE_SONG';
export const UPDATE_SONG_SUCCESS = 'songs/UPDATE_SONG_SUCCESS';
export const UPDATE_SONG_FAILED = 'songs/UPDATE_SONG_FAILED';

export const DELETE_SONG = 'songs/DELETE_SONG';
export const DELETE_SONG_SUCCESS = 'songs/DELETE_SONG_SUCCESS';
export const DELETE_SONG_FAILED = 'songs/DELETE_SONG_FAILED';

export const GET_SONG = 'songs/GET_SONG';
export const GET_SONG_SUCCESS = 'songs/GET_SONG_SUCCESS';
export const GET_SONG_FAILED = 'songs/GET_SONG_FAILED';
