export const SET_PLAYER_INSTANCE = 'midi/SET_PLAYER_INSTANCE';
export const PLAY_NOTE = 'midi/PLAY_NOTE';
export const STRUM_CHORD = 'midi/STRUM_CHORD';
export const REMOVE_FROM_QUEUE = 'midi/REMOVE_FROM_QUEUE';
