export const CREATE_CHORD = 'chords/CREATE_CHORD';
export const CREATE_CHORD_SUCCESS = 'chords/CREATE_CHORD_SUCCESS';
export const CREATE_CHORD_FAILED = 'chords/CREATE_CHORD_FAILED';

export const UPDATE_CHORD = 'chords/UPDATE_CHORD';
export const UPDATE_CHORD_SUCCESS = 'chords/UPDATE_CHORD_SUCCESS';
export const UPDATE_CHORD_FAILED = 'chords/UPDATE_CHORD_FAILED';

export const DELETE_CHORD = 'chords/DELETE_CHORD';
export const DELETE_CHORD_SUCCESS = 'chords/DELETE_CHORD_SUCCESS';
export const DELETE_CHORD_FAILED = 'chords/DELETE_CHORD_FAILED';

export const GET_CHORD = 'chords/GET_CHORD';
export const GET_CHORD_SUCCESS = 'chords/GET_CHORD_SUCCESS';
export const GET_CHORD_FAILED = 'chords/GET_CHORD_FAILED';
