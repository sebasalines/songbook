export const CREATE_SECTION = 'sections/CREATE_SECTION';
export const CREATE_SECTION_SUCCESS = 'sections/CREATE_SECTION_SUCCESS';
export const CREATE_SECTION_FAILED = 'sections/CREATE_SECTION_FAILED';

export const UPDATE_SECTION = 'sections/UPDATE_SECTION';
export const UPDATE_SECTION_SUCCESS = 'sections/UPDATE_SECTION_SUCCESS';
export const UPDATE_SECTION_FAILED = 'sections/UPDATE_SECTION_FAILED';

export const DELETE_SECTION = 'sections/DELETE_SECTION';
export const DELETE_SECTION_SUCCESS = 'sections/DELETE_SECTION_SUCCESS';
export const DELETE_SECTION_FAILED = 'sections/DELETE_SECTION_FAILED';

export const GET_SECTION = 'sections/GET_SECTION';
export const GET_SECTION_SUCCESS = 'sections/GET_SECTION_SUCCESS';
export const GET_SECTION_FAILED = 'sections/GET_SECTION_FAILED';
