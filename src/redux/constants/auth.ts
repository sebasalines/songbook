export const LOGOUT = 'auth/LOGOUT';

export const GET_USER_INFO = 'auth/GET_USER_INFO';
export const GET_USER_INFO_FAILED = 'auth/GET_USER_INFO_FAILED';
export const GET_USER_INFO_SUCCESS = 'auth/GET_USER_INFO_SUCCESS';