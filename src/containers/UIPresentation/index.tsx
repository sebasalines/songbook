import * as React from 'react';
import { connect } from 'react-redux';
import ChordList from '../../components/Chords/ChordList';
import { Container, Content, PrimaryButton, Page } from '../../components/common/UI';
import Button from '../../components/common/Button';
import Input from '../../components/common/Input';
import Heading from '../../components/common/Heading';
import colors from '../../constants/colors';
import { deleteChord } from '../../redux/actions/chords';

const Headings = () => (
  <Page>
    <Heading as="h1">Heading 1</Heading>
    <Heading color={colors.accent} accentColor={colors.lightBlue} as="h2">Heading 2 with custom colors</Heading>
    <Heading as="h3">Heading 3</Heading>
    <Heading fontSize="33px" as="h4">Heading 4 with custom size</Heading>
    <Heading as="h5">Heading 5</Heading>
  </Page>
);

const Inputs = () => (
  <>
    <Page flex>
      <Input placeholder="Test placeholder..." horizontal />
      <Input placeholder="Test placeholder..." />
      <Button flex>Save</Button>
    </Page>
    <Page flex style={{ flexDirection: 'column' }}>
      <Input placeholder="Test placeholder..." />
      <Input placeholder="Test placeholder..." />
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        <Button color={colors.accent} flex>Cancel</Button>
        <Button color={colors.secondary} flex>Save</Button>
      </div>
    </Page>
  </>
)

class UIPresentation extends React.Component<any, any> {
  deleteAllChords = () => {
    this.props.chords.map(itm => this.props.deleteChord(itm));
  }
  render() {
    return (
      <Container>
        <Content>
          <Heading as="h1">Borrar Todos los {this.props.chords.length} acordes: <Button icon="times-circle" onClick={this.deleteAllChords} /></Heading>
          <Headings />
          <Inputs />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  chords: state.chordReducer.chords,
});
const mapDispatchToProps = dispatch => ({
  deleteChord: chord => dispatch(deleteChord(chord)),
});
export default connect(mapStateToProps, mapDispatchToProps)(UIPresentation);
