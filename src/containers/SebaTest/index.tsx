import * as React from 'react';
import { connect } from 'react-redux';
import ChordList from '../../components/Chords/ChordList';
import { Container, Content, PrimaryButton, Page } from '../../components/common/UI';
import Button from '../../components/common/Button';
import Heading from '../../components/common/Heading';

const Headings = () => (
  <Page>
    <Heading as="h1">Heading 1</Heading>
    <Heading color="#150578" as="h2">Heading 2</Heading>
    <Heading as="h3">Heading 3</Heading>
    <Heading fontSize="33px" as="h4">Heading 4</Heading>
    <Heading as="h5">Heading 5</Heading>
  </Page>
);

const Buttons = () => (
  <Page>
  </Page>
)

class SebaTestContainer extends React.Component<any, any> {
  render() {
    return (
      <Container>
        <Content>
          <Headings />
          <Buttons />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  chords: state.chordReducer.chords,
})
export default connect(mapStateToProps)(SebaTestContainer);
