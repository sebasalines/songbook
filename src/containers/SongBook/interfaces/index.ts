import { ActiveNoteTransaction } from "../../../components/GuitarNeck";
import {
  SongChord,
  ChordList
} from '../../../models/chord';

export interface Props {
  addChord: (chord: SongChord) => void;
  deleteChord: (chord: SongChord) => void;
  updateChord: (chord: SongChord) => void;
  chordList: ChordList;
  history: any;
}

export interface State {
  chords: SongChord[];
}
