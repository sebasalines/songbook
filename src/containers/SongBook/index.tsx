import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import uuid from 'uuid/v4';
import GuitarNeck, { ActiveNoteTransaction } from '../../components/GuitarNeck';
import { chords } from 'tonal-scale';
import { Props, State } from './interfaces';
import { SongChord } from '../../models';
import styled from '../../helpers/styled';
import SongBookChord from './components/SongChord';
import * as actions from '../../redux/actions/chords';


const Container = styled.div`
  background-color: ${props => props.theme.colors.lightBlue};
  flex: 1;
  display: flex;
`;

const Content = styled.div`
margin: 20px auto;
padding: 20px;
width: 80vw;
position: relative;
`;

const AddButton = styled.button`
position: absolute;
top: 0;
right: 20px;
height: 24px;
min-width: 24px;
border-radius: 12px;
`;

const ChordsList = styled.div`
display: flex;
flex-wrap: wrap;
flex-grow: 2;
flex-direction: column;
justify-content: center;
`;

interface SongBookSong {
  name: string;
  sections: any;
}

class SongBook extends React.Component<Props, State> {
  contextRef: any;

  constructor(props: Props) {
    super(props);
  }

  addChord = () => {
    this.props.addChord({
      id: uuid(),
      name: '',
      firstFret: 1,
      numberOfFrets: 4,
      activeNotes: [],
    });
  }

  deleteChord = (id: string) => {
    const chord = _.find(this.props.chordList.chords, { id });
    chord ? this.props.deleteChord(chord) : console.error('Chord Not Found');
  }

  onChangeChordName = (id: string, name: string) => {
    const chord = _.find(this.props.chordList.chords, { id });
    if (chord) {
      chord.name = name;
      this.props.updateChord(chord);
    }
  }

  onChangeFirstFret = (id: string, firstFret: number) => {
    const chord = _.find(this.props.chordList.chords, { id });
    if (chord) {
      chord.firstFret = firstFret;
      this.props.updateChord(chord);
    }
  }

  onChangeFretNumber = (id: string, numberOfFrets: number) => {
    const chord = _.find(this.props.chordList.chords, { id });
    console.log(chord);
    if (chord) {
      chord.numberOfFrets = numberOfFrets;
      this.props.updateChord(chord);
    }
  }

  onChangeChord = (id: string, activeNotes: ActiveNoteTransaction) => {
    const chord = _.find(this.props.chordList.chords, { id });
    if (chord) {
      // chord.activeNotes = activeNotes;
      this.props.updateChord(chord);
    }
  }

  chords = () => {
    return (
      <ChordsList>
        {
          this.props.chordList.chords.map((chord: SongChord, index) => (
            <SongBookChord
              isEditing={index === 0}
              key={`chord_list_${chord.id}`}
              chord={chord}
              onDelete={this.deleteChord}
              onChangeName={this.onChangeChordName}
              onChangeNotes={this.onChangeChord}
              onChangeFirstFret={this.onChangeFirstFret}
              onChangeNumberOfFrets={this.onChangeFretNumber}
            />
          ))
        }
      </ChordsList>
    )
  }

  render() {
    return (
      <Container>
        <Content>
          <AddButton onClick={this.addChord}>+ Add Chord</AddButton>
          {this.chords()}
        </Content>
      </Container>
    );
  }
}
const mapStateToProps = state => ({ // funciones para mapear el estado de la aplicacion
  chordList: state.chordReducer,
});
const mapDispatchToProps = dispatch => ({ // funciones para mapear acciones de la aplicacion (dispatch an action to the application state)
  addChord: (chord) => dispatch(actions.ChordActions.createChord(chord)),
  deleteChord: (chord) => dispatch(actions.ChordActions.deleteChord(chord)),
  updateChord: (chord) => dispatch(actions.ChordActions.updateChord(chord)),
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SongBook))
