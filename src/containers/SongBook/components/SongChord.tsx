import * as React from 'react';
import GuitarNeck, { ActiveNoteTransaction } from '../../../components/GuitarNeck';
import { SongChord } from '../../../models';
import colors from '../../../constants/colors';
import styled from '../../../helpers/styled';
import {
  ChordPreview,
  FieldContainer,
  Label,
  Input,
  ChordCard,
  DeleteButton,
  Actions,
  ChordName,
  PrimaryButton,
  FieldProps,
} from  '../../../components/common/UI';

const Field: React.FunctionComponent<FieldProps> = props => {
  const { label, ...inputProps } = props;
  return (
    <FieldContainer>
      {
        label &&
        <Label>{label}</Label>
      }
      <Input {...props as any} />
    </FieldContainer>
  );
}

interface Props {
  isEditing: boolean;
  chord: SongChord;
  style?: React.CSSProperties;
  onDelete: (id: string) => void;
  onChangeNotes: (id: string, data: ActiveNoteTransaction) => void;
  onChangeName: (id: string, name: string) => void;
  onChangeFirstFret: (id: string, fret: number) => void;
  onChangeNumberOfFrets: (id: string, numberOfFrets: number) => void;
}

class SongBookChord extends React.PureComponent<Props, { isEditing: boolean }> {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: props.isEditing,
    };
  }

  onDelete = () => this.props.onDelete(this.props.chord.id)

  onChangeNotes = (data: ActiveNoteTransaction) => this.props.onChangeNotes(this.props.chord.id, data);

  onChangeName = (e: React.ChangeEvent<HTMLInputElement>) => this.props.onChangeName(this.props.chord.id, e.currentTarget.value);

  onChangeFirstFret = (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => this.props.onChangeFirstFret(this.props.chord.id, Number(e.currentTarget.value));

  onChangeFretNumber = (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => this.props.onChangeNumberOfFrets(this.props.chord.id, Number(e.currentTarget.value));

  toggleEditing = () => {
    this.setState({ isEditing: !this.state.isEditing });
  }

  render() {
    const { chord } = this.props;
    const fretCount = chord.numberOfFrets || 6;
    return (
      <ChordCard style={this.props.style}>
        <DeleteButton onClick={this.onDelete}>X</DeleteButton>
        <ChordPreview numberOfFrets={fretCount}>
          {/* <GuitarNeck
            isEditable={this.state.isEditing}
            name={chord.name}
            numberOfFrets={fretCount}
            firstFret={chord.firstFret}
            onDelete={this.onDelete}
            onChange={this.onChangeNotes}
            activeNotes={chord.activeNotes}
          /> */}
        </ChordPreview>
        {
          this.state.isEditing ? (
            <Actions>
              <Field label="Nombre" type="text" onChange={this.onChangeName} value={chord.name} />
              <Field label="Primer traste" type="number" max="12" onChange={this.onChangeFirstFret} value={chord.firstFret} />
              <Field label="Trastes" type="number" max="12" onChange={this.onChangeFretNumber} value={chord.numberOfFrets} />
              <Field type="submit" onClick={this.toggleEditing} value="Save" />
            </Actions>
          ) : (
              <Actions>
                <ChordName>{chord.name}</ChordName>
                <PrimaryButton onClick={this.toggleEditing}>Edit</PrimaryButton>
              </Actions>
            )
        }
      </ChordCard>
    );
  }
}
export default SongBookChord;
