import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Props, State } from './interfaces';
import Section from './components/Section';


export class SectionsContainer extends React.Component<Props,State> {

  constructor(props) {
    super(props);
    
  }

  render() {
    return (
      <div>
        <Section />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  sections: state.sectionReducer,
})

const mapDispatchToProps = ({
});

export default connect(mapStateToProps, mapDispatchToProps)(SectionsContainer)
