import * as React from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import MIDISounds from 'midi-sounds-react';
import styled from '../../helpers/styled';
import themeColor from '../../helpers/themeColor';
import ProgressionScreen from '../../screens/ProgressionScreen';
import SongsContainer from '../../containers/Songs';
import SongEditorScreen from '../../screens/SongEditorScreen';
import SectionEditorScreen from '../../screens/SectionEditorScreen';
import ChordEditor from '../../components/Chords/ChordEditor';
import UIPresentation from '../UIPresentation';
import ProgressionEditorScreen from '../../screens/ProgressionEditorScreen';
import { setPlayer } from '../../redux/actions/midi';
import { connect } from 'react-redux';
import Player from '../../components/common/Player';

const Header = styled.header`
display: flex;
flex-direction: row;
align-items: center;
padding: 0 20px;
.brand {
  color: ${themeColor('white')};
  font-size: 24px;
}
.content {
  flex: 1;
}
.profile {
  
}
`;
const Sidebar = styled.div`
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: stretch;
padding: 20px;
a {
  padding: 10px 0;
  text-decoration: none;
  color: ${themeColor('white')};
}
`;
const Content = styled.div``;
const Container = styled.div`
flex: 1;
display: grid;
grid-template-columns: 14vw 86vw;
grid-template-rows: 50px auto;
grid-template-areas: "header header"
			"sidebar main";
${Header} {
  grid-area: header;
  background-color: ${themeColor('primary')};
}
${Sidebar} {
  grid-area: sidebar;
  background-color: ${themeColor('lightBlue')};
}
${Content} {
  grid-area: main;
  background-color: ${themeColor('white')};
  padding: 0 3vw;
}
`;
class AppLayout extends React.Component<any> {
  setPlayerInstance = ref => this.props.setPlayerInstance(ref);
  render() {
    return (
      <Container>
        <Header>
          <div className="brand">SongBook</div>
          <div className="content"></div>
          <div className="profile">
            <Player />
          </div>
        </Header>
        <Sidebar>
          <a href="#/">Inicio</a>
          <a href="#/songs">Canciones</a>
          <a href="#/chord-editor">Editor de Acordes</a>
          <a href="#/progressions">Progresiones</a>
        </Sidebar>
        <Content>
          <Router basename={process.env.PUBLIC_URL}>
            <Switch>
              <Route exact path="/" component={UIPresentation} />
              <Route exact path="/progressions" component={ProgressionScreen} />
              <Route exact path="/songs" component={SongsContainer} />
              <Route exact path="/song-editor" component={SongEditorScreen} />
              <Route exact path="/progression-editor" component={ProgressionEditorScreen} />
              <Route exact path="/section-editor" component={SectionEditorScreen} />
              <Route exact path="/chord-editor" component={ChordEditor} />
            </Switch>
          </Router>
        </Content>
      </Container>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  setPlayerInstance: player => dispatch(setPlayer(player)),
});
export default connect(null, mapDispatchToProps)(AppLayout);
