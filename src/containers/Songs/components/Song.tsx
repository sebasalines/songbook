import React, { Component } from 'react';
import {
  ListElement,
  Content,
  PrimaryButton,
  DangerPrimaryButton,
  ListElementDetails,

} from '../../../components/common/UI';
import Heading from '../../../components/common/Heading';
import Button from '../../../components/common/Button';
import {
  SongInterface,
} from '../../../models'
import colors from '../../../constants/colors';

interface Props {
  song: SongInterface;
  history: any;
  deleteSong: (song: SongInterface) => void;
}

interface State {
  isOpenDetails: boolean;
}

class Song extends Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      isOpenDetails: false,
    }
  }

  openDetails = () => {
    this.setState(prevState => ({
      isOpenDetails: !prevState.isOpenDetails
    }));
  }

  editCurrentSong = () => {
    this.props.history.push({
      pathname: '/song-editor',
      state: { song: this.props.song }
    });
  }

  deleteCurrentSong = () => {
    this.props.deleteSong(this.props.song);
  }

  render() {
    return (
      <React.Fragment>
        <ListElement
          onClick={this.openDetails}
        >
          {
            this.props.song.name
          }
        </ListElement>
        {
          this.state.isOpenDetails &&
          <ListElementDetails>
            <Heading as="h3">Details: </Heading>
            {
              this.props.song.id
            }
            <div>
              <PrimaryButton
                onClick={this.editCurrentSong}
              >
                Edit Song
            </PrimaryButton>
              <DangerPrimaryButton onClick={this.deleteCurrentSong}>
                Delete
            </DangerPrimaryButton>
            </div>
          </ListElementDetails>
        }
      </React.Fragment>
    )
  }
}

export default Song;