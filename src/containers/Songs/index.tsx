import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Props, State } from './interfaces';
import {
  Container,
  Content,
  UnorderedList,
  LightGreenButton,
} from '../../components/common/UI';
import SongCreationModal from '../../modals/SongCreationModal';
import Song from './components/Song';
import { SongActions } from '../../redux/actions/songs';

export class SongContainer extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    
    this.state = {
      isSongsModalOpen: false,
    }
  }

  toggleSongsModal = () => {
    this.setState( prevState => ({
      isSongsModalOpen: !prevState.isSongsModalOpen
    }));
  }

  render() {
    return (
      <Container>
        <Content>
          <LightGreenButton
            onClick={this.toggleSongsModal}

          >
            Add Song
          </LightGreenButton>
          <UnorderedList>
            {
              this.props.playlist.songs.map( song => (
                <Song
                  song={song}
                  history={this.props.history}
                  deleteSong={this.props.deleteSong}
                />
              ))
            }
          </UnorderedList>
          <SongCreationModal
            isModalOpen={this.state.isSongsModalOpen}
            toggleModal={this.toggleSongsModal}
            createSong={this.props.createSong}
          />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  playlist: state.songReducer,
});

const mapDispatchToProps = dispatch => ({
  createSong: (song) => dispatch(SongActions.createSong(song)),
  deleteSong: (song) => dispatch(SongActions.deleteSong(song)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongContainer)
