import {
  SongChord,
  SongList,
  SongInterface,
} from '../../../models';

export interface Props {
  playlist: SongList;
  history: any;
  createSong: (song: SongInterface) => void;
  deleteSong: (song: SongInterface) => void;
}

export interface State {
  isSongsModalOpen: boolean;
}