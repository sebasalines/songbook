import React, { Component } from 'react';
import styled from 'styled-components';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/es/integration/react';
import { Provider } from 'react-redux';
import configureStore from './redux/store/configStore';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContextProvider } from 'react-dnd';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fas)

import SebaTestContainer from './containers/SebaTest';
import LucasTestContainer from './containers/LucasTest';
import { ThemeProvider } from './helpers/styled';
import theme from './constants/theme';
import SongBook from './containers/SongBook';
import AppLayout from './containers/AppLayout';

const AppContainer = styled.div`
  flex: 1;
  display: flex;
  background-color: ${props => props.theme.colors.secondary};
`;

const persistor = persistStore(configureStore);

class App extends Component {
  render() {
    return (
      <Provider store={configureStore}>
        <PersistGate loading={null} persistor={persistor}>
          <DragDropContextProvider backend={HTML5Backend}>
            <ThemeProvider theme={theme}>
              <Router basename="/songbook">
                <Switch>
                  <Route path="/" component={AppLayout} />
                  <Route exact path="/seba-test" component={SebaTestContainer} />
                  <Route exact path="/lucas-test" component={LucasTestContainer} />
                </Switch>
              </Router>
            </ThemeProvider>
          </DragDropContextProvider>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
