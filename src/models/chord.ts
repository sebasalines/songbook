import { ActiveNoteInfo } from '../components/Chords/ChordEditor';

export interface ChordList {
    name: string;
    chords: SongChord[];
  }

  export type SongChord = {
    id: string;
    name: string;
    firstFret?: number;
    numberOfFrets?: number;
    activeNotes: ActiveNoteInfo[];
  }
  