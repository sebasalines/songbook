import {
    ChordList,
} from './chord'
// Progression Interfaces

export interface ProgressionInterface {
    id: string;
    name: string;
    chordList: ChordList;
  }
  
  export interface ProgressionsList{
    name: string;
    progressions: ProgressionInterface[];
  }