import {
    Measure,
    TimeSignature,
} from './note';
import {
    ProgressionInterface,
} from './progression';

export interface SectionInterface {
    id: string;
    label: string;
    measures: Measure[];
    timeSignature: TimeSignature;
    progression: ProgressionInterface;
  }

export interface SectionList {
    id: string;
    name: string;
    sections: SectionInterface[];
}