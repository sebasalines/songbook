export interface TimeSignature {
    quarters: number;
    measure: number;
  }
  
  export enum NoteDurations {
    Whole = 4 / 4, // 1
    Half = 2 / 4, // 0.5
    Quarter = 1 / 4, // 0.25
    Eigth = 1 / 8, // 0.125
    Sixteenth = 1 / 16, // 0.0625
  }
  
  export interface Note {
    pitch: string;
    duration: NoteDurations;
  }
  
  export interface Measure {
    notes: Note[];
    timeSignature?: TimeSignature;
  }
  