import { SongChord } from "./chord";
import { MidiReducer } from "../redux/reducers/midi";

export interface ChordsReducer {
  chords: SongChord[];
}

export interface AppState {
  chordReducer: ChordsReducer;
  songReducer: any;
  progressionReducer: any;
  midi: MidiReducer;
}