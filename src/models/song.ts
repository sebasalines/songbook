import {
    SectionInterface,
    SectionList,
} from './section';

export interface SongList {
    name: string;
    songs: SongInterface[];
  }
  
  export interface SongInterface {
    id: string;
    name: string;
    sections: SectionList;
  }
  
