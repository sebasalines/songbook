import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ProgressionActions } from '../redux/actions/progressions';
import {
  ProgressionInterface,
  ChordList,
  SongChord,
} from '../models';
import Progression from '../components/Progressions/Progression';
import {
  HorizontalList,
  ListElement,
  Page,
} from '../components/common/UI'
import Heading from '../components/common/Heading';
import { listenerCount } from 'cluster';
import { chord } from 'tonal-dictionary';
import ChordPreview from '../components/Chords/ChordPreview';

interface Props {
  updateProgression: (prog: ProgressionInterface) => void;
  history: any;
  location: any;
  chords: ChordList;
}

interface State {
  progression: ProgressionInterface;
}

export class ProgressionEditorScreen extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    const locationState = this.props.location.state ? this.props.location.state : null

    
    this.state = {
      progression: locationState ? locationState.progression : null,
    }
  }

  addChordToProgression = (chord: SongChord) => {
    this.setState( prevState => ({
      progression: {
        ...prevState.progression,
        chordList: {
          ...prevState.progression.chordList,
          chords: [
            ...prevState.progression.chordList.chords,
            chord
          ]
        }
      }
    }),
    () => this.props.updateProgression(this.state.progression)
    );
    
  }

  deleteChordFromProgression = (chord: SongChord, listIndex: number) => {
    console.log(this.state.progression.chordList.chords);
    this.setState( prevState => ({
      progression: {
        ...prevState.progression,
        chordList: {
          ...prevState.progression.chordList,
          chords: [
            ...prevState.progression.chordList.chords.filter(
              (itm, index) => itm.id !== chord.id || index !== listIndex)
          ]
        }
      }
    }),
    () => this.props.updateProgression(this.state.progression)
    );
  }

  render() {
    return (
      <React.Fragment>
        {this.state.progression ?
          <Page>
            <Heading as="h2">{this.state.progression.name}</Heading>
            <HorizontalList>
              {
                this.state.progression.chordList.chords.map( (chord, index) => (
                  <ListElement
                    key={`ChordList_${index}_${chord.id}`}
                    onClick={() => this.deleteChordFromProgression(chord, index)}
                  >
                    <ChordPreview chord={chord} />
                  </ListElement>
                ))
              }
            </HorizontalList>

            <Heading as="h2">Chords Available:</Heading>
            <HorizontalList>
              {
                this.props.chords.chords.map( (chord, index) => (
                  <ListElement
                    key={index}
                    onClick={() => this.addChordToProgression(chord)}
                  >
                    <ChordPreview chord={chord} />
                  </ListElement>
                ))
              }
            </HorizontalList>
          </Page>
          :
          this.props.history.push('/progressions')
        }
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  chords: state.chordReducer,
})

const mapDispatchToProps = dispatch => ({
  updateProgression: (prog) => dispatch(ProgressionActions.updateProgression(prog)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProgressionEditorScreen)
