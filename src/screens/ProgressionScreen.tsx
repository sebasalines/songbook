import * as React from 'react';
import { connect } from 'react-redux';
import * as actions from '../redux/actions/progressions';
import { withRouter } from 'react-router-dom';
import {
  UnorderedList,
  ListElement,
  Container,
  Content,
  LightGreenButton,
} from '../components/common/UI';
import {
  ProgressionsList,
  ProgressionInterface,
} from '../models';
import ProgressionModal from '../modals/ProgressionModal';


interface Props {
  progList: ProgressionsList;
  createProgression: (prog: ProgressionInterface) => void;
  deleteProgression: (prog: ProgressionInterface) => void;
  updateProgression: (prog: ProgressionInterface) => void;
  history: any;
}

interface State {
  modalIsOpen: boolean;
}

class ProgressionsScreen extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      modalIsOpen: false
    }
  }

  openProgressionModal = () => {
    this.setState({ modalIsOpen: true });
  }

  addProgression = (prog: ProgressionInterface) => {
    this.props.createProgression(prog);
  }

  closeModal = () => {
    this.setState(state => ({
      modalIsOpen: !state.modalIsOpen
    }));
  }

  openProgressionEditor = (prog: ProgressionInterface) => {
    this.props.history.push({
      pathname: '/progression-editor',
      state: { progression: prog }
    });
  }

  render() {
    return (
      <Container>
        <Content>
          <UnorderedList>
            {
              this.props.progList.progressions.map(itm => (
                <ListElement onClick={() => this.openProgressionEditor(itm)}>
                  {itm.name}
                </ListElement>
              ))
            }
          </UnorderedList>
          <LightGreenButton onClick={this.openProgressionModal}>
            Add Progression
           </LightGreenButton>

          <ProgressionModal
            modalIsOpen={this.state.modalIsOpen}
            closeModal={this.closeModal}
            addProg={this.addProgression}
          />
        </Content>
      </Container>

    );
  }
}

const mapStateToProps = state => ({
  progList: state.progressionReducer
});

const mapDispatchToProps = dispatch => ({
  createProgression: (prog) => dispatch(actions.ProgressionActions.createProgression(prog)),
  deleteProgression: (prog) => dispatch(actions.ProgressionActions.deleteProgression(prog)),
  updateProgression: (prog) => dispatch(actions.ProgressionActions.updateProgression(prog)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProgressionsScreen));