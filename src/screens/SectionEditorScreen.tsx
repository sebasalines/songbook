import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  SongInterface,
  SectionList,
  ProgressionsList,
  SectionInterface,
  ProgressionInterface,
  Measure,
} from '../models';
import {
  Page,
  HorizontalList,
  ListElement,
} from '../components/common/UI';
import Heading from '../components/common/Heading';
import { withRouter } from 'react-router-dom';
import { SongActions } from '../redux/actions/songs'
import Section from '../containers/Sections/components/Section';
import uuid from 'uuid/v4';
import progressionReducer from '../redux/reducers/progressions';

interface Props {
  song: SongInterface;
  location: any;
  history: any;
  progressions: ProgressionsList;
  updateSong: (song: SongInterface) => void;
}

interface State {
  song: SongInterface;

}

export class SectionEditorScreen extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    const locationState = this.props.location.state ? this.props.location.state : null
    console.log(this.props.progressions.progressions);

    this.state = {
      song: locationState ? locationState.song : null,
    }
  }

  addSectionToSong = (
    song: SongInterface,
    prog: ProgressionInterface
    ) => {
    const newSection = {
      id: uuid(),
      label: prog.name,
      measures: [],
      timeSignature: {
        quarters: 2,
        measure: 2
      },
      progression: prog
    }

    this.setState( prevState => ({
      song: {
        ...prevState.song,
        sections: {
          ...prevState.song.sections,
          sections: [
            ...prevState.song.sections.sections,
            newSection
          ]
        }       
      }
    }), () => this.props.updateSong(this.state.song));
  }

  deleteSectionFromSong = (song: SongInterface, section: SectionInterface, listIndex: number) => {
    this.setState( prevState => ({
      song: {
        ...prevState.song,
        sections: {
          ...prevState.song.sections,
          sections: [
            ...prevState.song.sections.sections.filter(
              (itm, index) => itm.id !== section.id || index !== listIndex
              )
          ]
        }       
      }
    }), () => this.props.updateSong(this.state.song));
  }

  render() {
    return (
      <React.Fragment>
        {this.props.location.state ?
          <Page>
          <Heading as="h2">Section Editor</Heading>
          <p>{this.state.song.sections.name}</p>
          <HorizontalList>
            {
              this.state.song.sections.sections.map( (section, index) => (
                <ListElement
                  key={`SectionList_${section.id}_${index}`}
                  onClick={() => this.deleteSectionFromSong(this.state.song, section, index)}
                >
                  {
                    section.label
                  }
                </ListElement>
              ))
            }
          </HorizontalList>

          <Heading as="h2">Progressions: </Heading>
          <HorizontalList>
            {
              this.props.progressions.progressions.map( (prog,index) => (
                <ListElement
                  key={`ProgressionList_${prog.id}_${index}`}
                  onClick={() => this.addSectionToSong(this.state.song,prog)}
                >
                  {
                    prog.name
                  }
                </ListElement>
              ))
            }
          </HorizontalList>

          </Page>
          :
          this.props.history.goBack()
        }
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  progressions: state.progressionReducer,
})

const mapDispatchToProps = dispatch => ({
  updateSong: (song) => dispatch(SongActions.updateSong(song)),
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SectionEditorScreen))
