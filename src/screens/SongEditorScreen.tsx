import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  SongInterface,
} from '../models';
import {
  Page,
  PrimaryButton,
  DangerPrimaryButton,
  HorizontalList,
  ListElement,

} from '../components/common/UI';
import Heading from '../components/common/Heading';
import Input from '../components/common/Input';
import { SongActions } from '../redux/actions/songs';


interface Props {
  history: any;
  location: any;
  updateSong: (song: SongInterface) => void;
}

interface State {
  song: SongInterface;
  songName: string;
}

export class SongEditorScreen extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      song: this.props.location.state ? this.props.location.state.song : null,
      songName: this.props.location.state ? this.props.location.state.song.name : '',
    }
  }

  saveUpdatedSong = () => {
    const updatedSong = {
      ...this.state.song,
      name: this.state.songName
    }
    this.props.updateSong(updatedSong);
    this.props.history.goBack();
  }

  goBack = () => {
    this.props.history.goBack();
  }

  onChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      songName: e.target.value
    });
  }

  navigateSectionEditor = () => {
    this.props.history.push({
      pathname: '/section-editor',
      state: {
        song: this.state.song
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state.song ?
          <Page>
            <Heading as="h2">Song name:</Heading>
            <Input value={this.state.songName} onChange={this.onChangeName} horizontal />
            <Heading as="h2">Sections: </Heading>
            <HorizontalList
              onClick={this.navigateSectionEditor}
            >
              {
                this.state.song.sections.sections.map(section => (
                  <ListElement>
                    {section.label}
                  </ListElement>
                ))
              }
            </HorizontalList>

            <div>
              <PrimaryButton
                onClick={this.saveUpdatedSong}
              >
                Save
          </PrimaryButton>
              <DangerPrimaryButton
                onClick={this.goBack}
              >
                Cancel
          </DangerPrimaryButton>
            </div>
          </Page>
          :
          this.props.history.push('/songs')
       }
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = dispatch => ({
  updateSong: (song) => dispatch(SongActions.updateSong(song)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SongEditorScreen)
