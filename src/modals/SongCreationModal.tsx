import React, { Component, ReactElement } from 'react';
import Modal from 'react-modal';
import {
  Container,
  Content,
  LightRedButton,
  Page,
  PrimaryButton,
  DangerPrimaryButton,

} from '../components/common/UI';
import Input from '../components/common/Input';
import Heading from '../components/common/Heading';
import uuid from 'uuid/v4';
import {
  SongInterface
} from '../models';


interface Props {
  isModalOpen: boolean;
  toggleModal: () => void;
  createSong: (song: SongInterface) => void;
}

interface State {
  songName: string;
}

class SongCreationModal extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      songName: '',
    }
  }

  createSong = () => {
    this.state.songName === '' ? alert("Please introduce songname") :
      this.props.createSong({
        id: uuid(),
        name: this.state.songName,
        sections: {
          id: uuid(),
          name: 'Default List',
          sections: []
        },
      });
    this.props.toggleModal();
  }

  onChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      songName: e.target.value
    });
  }

  render() {
    return (
      <Modal isOpen={this.props.isModalOpen}>
        <Page>
          <Heading as="h4">Name for the song: </Heading>
          <Input onChange={this.onChangeName} placeholder="Song name..." horizontal />
          <Heading as="h4">Created by:</Heading>

          <div>
            <PrimaryButton
              onClick={this.createSong}
            >
              Add song
            </PrimaryButton>
            <DangerPrimaryButton
              onClick={this.props.toggleModal}
            >
              Cancel
            </DangerPrimaryButton>
          </div>
        </Page>
      </Modal>
    )
  }
}

export default SongCreationModal;