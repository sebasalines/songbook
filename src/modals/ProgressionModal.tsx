import * as React from 'react';
import Modal from 'react-modal';
import { ProgressionInterface } from '../models';
import {
  PrimaryButton,
  LightGreenButton,
  LightRedButton,
  Container,
  Content,
  Input,
} from '../components/common/UI';
import uuid from 'uuid/v4';

interface Props {
  modalIsOpen: boolean;
  closeModal: () => void;
  addProg: (prog: ProgressionInterface) => void;
}

interface State {
  name: string;
}

class ProgressionModal extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      name: ''
    }
  }

  handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ name: e.target.value });
  }

  addProg = () => {

    this.state.name === '' ? alert('You must add a progression Name') :
      this.props.addProg({
        id: uuid(),
        name: this.state.name,
        chordList: {
          name: this.state.name + ' Chords',
          chords: []
        },
      });

    this.props.closeModal();
  }

  render() {
    return (
      <Modal isOpen={this.props.modalIsOpen}>
        <h4>Add a new Progression:</h4>
        <Input onChange={this.handleNameChange} />
        <LightGreenButton
          onClick={this.addProg}
        >
          Add Progression
                </LightGreenButton>
        <LightRedButton onClick={this.props.closeModal}>Cancel</LightRedButton>
      </Modal>
    );
  }
}

export default ProgressionModal;