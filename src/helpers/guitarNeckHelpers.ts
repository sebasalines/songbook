import * as _ from 'lodash';
import * as Tonal from 'tonal';
import * as Range from 'tonal-range';
import { ActiveNoteInfo } from '../components/Chords/ChordEditor';

export const defaultStrings: _.Dictionary<{ start: string, end: string }> = {
  // "8": { start: 'D2', end: 'D4' },
  // "7": { start: 'A2', end: 'A4' },
  "6": { start: 'E2', end: 'E4' },
  "5": { start: 'A2', end: 'A4' },
  "4": { start: 'D3', end: 'D5' },
  "3": { start: 'G3', end: 'G5' },
  "2": { start: 'B3', end: 'B5' },
  "1": { start: 'E4', end: 'E6' },
};

export interface PitchInfo {
  midi: number | null;
  flat: string;
  sharp: string;
}

export interface FretInfo {
  fret: number;
  pitch: PitchInfo;
}

export interface StringInfo {
  string: number;
  frets: FretInfo[];
}

export interface NeckOptions {
  mirror?: {
    x?: boolean;
    y?: boolean;
  };
  includeOpenString?: boolean;
  fromFret?: number;
  toFret?: number;
  numberOfFrets?: number;
};

export const getNeck = (opts?: NeckOptions): StringInfo[] => {
  const mirrorX: boolean = _.get(opts, 'mirror.x', false);
  const mirrorY: boolean = _.get(opts, 'mirror.y', false);
  const fromFret = _.get(opts, 'fromFret', 0); // default start from open strings
  const toFret = _.get(opts, 'toFret', 12); // default start from open strings
  const strings = Object.keys(defaultStrings).map(str => {
    const itm = defaultStrings[str];
    const flatRange: string[] = Range.chromatic([itm.start, itm.end]);
    const sharpRange: string[] = Range.chromatic([itm.start, itm.end], true);
    const frets = _.chain([...flatRange])
      .map((flat, index) => {
        const sharp = sharpRange[index];
        const midi = Tonal.midi(flat);
        return {
          fret: index,
          pitch: {
            flat,
            sharp,
            midi,
          },
        };
      })
      .value();
    const fromIndex = frets.findIndex(itm => itm.fret === fromFret);
    const toIndex = frets.findIndex(itm => itm.fret === toFret);
    return {
      string: Number(str),
      frets: frets.slice(
        fromIndex,
        toIndex,
      ).sort((a, b) => {
        return mirrorX
          ? b.fret < a.fret ? 1 : -1
          : b.fret < a.fret ? -1 : 1;
      }),
    };
  });
  return mirrorY ? _.reverse(strings) : strings;
}

export const chordFretInfo = (notes: ActiveNoteInfo[]): { fromFret: number, toFret: number } => {
  const usedFrets = notes.reduce((frets: FretInfo[], itm) => {
    if (itm.note) {
      return [...frets, itm.note];
    }
    return frets;
  }, []);
  let fromFret = usedFrets.reduce((min, fret) => fret.fret < min ? fret.fret : min, usedFrets[0].fret)
  let toFret = usedFrets.reduce((max, fret) => fret.fret > max ? fret.fret : max, usedFrets[0].fret)
  fromFret = Math.abs(toFret - fromFret) <= 4 && fromFret <= 2
    ? 0 : fromFret;
  return {
    fromFret,
    toFret: Math.abs(toFret - fromFret) <= 4 ? fromFret + 4 : toFret,
  };
}

export const dotsForFret = (fret: number) => {
  if (
    fret === 3 ||
    fret === 5 ||
    fret === 7 ||
    fret === 9
  ) {
    return 1;
  }
  return fret === 12 ? 2 : 0;
}