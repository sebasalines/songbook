import { AppTheme } from "../constants/theme";
import { ThemeProps } from "styled-components";

const themeColor = (color: string) => (props: { theme: AppTheme }) => {
  return props.theme.colors[color] || '';
}
export default themeColor;
