export default {
  transparent: 'transparent',
  white: '#FFF7F9',
  black: '#04080F',
  primary: '#4200D1',
  secondary: '#001021',
  accent: '#D00000',
  lightBlue: '#3F88C5',
  page: '#F5F1EF',
}