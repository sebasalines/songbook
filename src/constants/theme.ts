import colors from "./colors";

export interface AppTheme {
  colors: typeof colors;
};

const theme: AppTheme = {
  colors,
};
export default theme;