import * as React from 'react';
import Progression from './Progression';
import { css } from 'aphrodite';
import styles from './styles';

class ProgressionsList extends React.Component {
	render(){
		return(
		<div className={css(styles.progressionList)}>
        	<Progression />
		</div>
		);
	}
}

export default ProgressionsList;