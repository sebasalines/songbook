import { StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
    progression: {
        width: '100px',
        height: '100px',
        display: 'inline-block',
        border: '1px dashed gray',
        padding: '0.5rem 1rem',
        backgroundColor: 'white',
        cursor: 'move',
    },
    progressionList: {
        display: 'flex',
        flex: 1,
        border : '1px solid black',
        backgroundColor: '#FF15FF',
    },
});

export default styles;