import * as React from 'react';
import { DragSource, ConnectDragSource } from 'react-dnd';
import { css } from 'aphrodite';
import styles from './styles';

const boxSource = {
  beginDrag(props) {
    return props;
  },
  endDrag(props, monitor) {
    console.log(props, monitor, monitor.getItem());
  },
}

export interface Props {
  connectDragSource: ConnectDragSource;
  prog: any;
  name?: boolean;
}

class Progression extends React.Component<Props>  {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const { connectDragSource } = this.props;
    return connectDragSource(
      <div className={css(styles.progression)}>{this.props.name}</div>
    );
  }
}

export default DragSource('box', boxSource, connect => ({
  connectDragSource: connect.dragSource(),
}))(Progression);