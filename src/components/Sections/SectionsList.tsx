import * as React from 'react';
import { connect } from 'react-redux';
import { css } from 'aphrodite';
import * as actions from '../../redux/actions';
import styles from './styles';
import { withRouter } from 'react-router-dom';
import Section from './Section';
import styled from 'styled-components';


class SectionsList extends React.Component {
	render(){
		return(
		<div className={css(styles.sectionList)}>
        <Section />
      </div>
		);
	}
}

const mapStateToProps = state => {
	sections: state.sectionReducer 
}

const mapDispatchToProps = dispatch => {

}

export default connect(mapStateToProps, mapDispatchToProps)(SectionsList);