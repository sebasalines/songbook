import * as React from 'react';
import { DropTarget, ConnectDropTarget, DropTargetMonitor } from 'react-dnd';
import Progression from '../Progressions/Progression';

function getStyle(backgroundColor: string): React.CSSProperties {
  return {
    border: '1px solid rgba(0,0,0,0.2)',
    minHeight: '8rem',
    minWidth: '8rem',
    color: 'white',
    backgroundColor,
    padding: '2rem',
    paddingTop: '1rem',
    margin: '1rem',
    textAlign: 'center',
    float: 'left',
    fontSize: '1rem',
  }
}

export interface SectionsListProps {
  greedy?: boolean;
}

interface SectionsListCollectedProps {
  isOver: boolean;
  isOverCurrent: boolean;
  connectDropTarget: ConnectDropTarget;
}

export interface SectionsListState {
  hasDropped: boolean;
  hasDroppedChild: boolean;
}

const boxTarget = {
  drop() {
    return { name: 'Dustbin' }
  },
}

class Section extends React.Component<
  SectionsListProps & SectionsListCollectedProps,
  SectionsListState
  > {

  constructor(props: SectionsListProps & SectionsListCollectedProps) {
    super(props);
    this.state = {
      hasDropped: false,
      hasDroppedChild: false,
    }
  }
  handleDrop = () => {
    return
  }
  render() {
    const {
      isOver,
      isOverCurrent,
      connectDropTarget,
      children,
    } = this.props;

    const {
      hasDropped,
      hasDroppedChild,
    } = this.state;

    let backgroundColor = 'rgba(0, 0, 0, .5)'

    if (isOverCurrent || isOver) {
      backgroundColor = 'darkgreen'
    }

    return connectDropTarget(
      <div style={getStyle(backgroundColor)}>
        {hasDropped &&
          <React.Fragment>
            <Progression name="coso" />
          </React.Fragment>
        }
      </div>
    );
  }
}

export default DropTarget('box', boxTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  isOverCurrent: monitor.isOver({ shallow: true }),
}))(Section);