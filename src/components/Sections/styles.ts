import { StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
    container:{},
    sectionList:{},
    section:{
        border: '1px solid rgba(0,0,0,0.2)',
		minHeight: '8rem',
		minWidth: '8rem',
		color: 'black',
		backgroundColor: 'white',
		padding: '2rem',
		paddingTop: '1rem',
		margin: '1rem',
		textAlign: 'center',
		float: 'left',
		fontSize: '1rem',
    }
});

export default styles;