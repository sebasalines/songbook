import { StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
  container: {
    margin: '30px',
    padding: '20px 0',
    width: '500px',
    height: '200px',
    display: 'flex',
    position: 'relative',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  stringsOverlay: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  string: {
    height: '3px',
    marginBottom: '40px',
    ':last-child': {
      marginBottom: '0',
    },
    position: 'relative',
    background: 'black',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    zIndex: 90,
  },
  fretsOverlay: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  openStringFret: {
    flex: 'none !important',
    width: '9px !important',
    minWidth: '9px !important',
    background: 'black !important',
    position: 'absolute',
    right: '-9px',
    height: '100%',
  },
  openStringFretBlack: {
    background: 'black !important',
  },
  openStringFretTransparent: {
    background: 'transparent !important',
  },
  fret: {
    background: 'white',
    flex: 1,
    position: 'relative',
    justifyContent: 'center',
    minWidth: '50px',
    borderRight: '2px solid black',
    ":last-child": {
      borderRight: '1px solid transparent',
    },
    ":first-child": {
      borderLeft: '2px solid black',
    },
  },
  fretLabel: {
    position: 'relative',
    textAlign: 'center',
    top: '-14px',
    height: '28px',
    width: '28px',
    background: 'rgba(0, 0, 0, .5)',
    zIndex: 99,
    color: 'white',
    lineHeight: '28px',
    fontSize: 12,
    borderRadius: '14px',
    border: '1px solid yellow',
  },
  fretLabelActive: { 
    color: 'black',
    background: 'yellow',
    borderColor: 'black',
  },
  dot: {
    margin: '5px 4px',
    // alignSelf: 'center',
    height: '10px',
    width: '10px',
    borderRadius: '10px',
    background: 'black',
  },
  dotsContainer: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column',
  }
}); 
export default styles;
