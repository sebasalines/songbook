import * as React from 'react';
import * as Tonal from 'tonal';
import { chromatic } from 'tonal-range';
import _, { Dictionary } from 'lodash';
import styles from './styles';
import { css } from 'aphrodite';
import styled from 'styled-components';
import colors from '../../constants/colors';
const ton = Tonal;

const Container = styled('div') <{ rotated?: boolean, scale?: string }>`
  transform: ${props => {
    let transform = '';
    if (props.rotated) {
      transform += 'rotate(-90deg) ';
    }
    if (props.scale) {
      transform += `scale(${props.scale}) `;
    }
    return transform;
  }};
  margin: 20px;
  min-width: 500px;
  max-width: 1000px;
  height: 200px;
  position: relative;
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: space-between;
`;

const NutContainer = styled.div`
flex: none !important;
width: 9px !important;
min-width: 9px !important;
background: black !important;
position: relative;
display: flex;
flex-direction: column;
justify-content: space-between;
left: 100%;
height: 100%;
`;

const GuitarNut = styled.div`

`;

const StringContainer = styled.div`
position: absolute;
height: 100%;
width: 100%;
display: flex;
flex-direction: column;
align-items: stretch;
justify-content: space-between;
`;

const String = styled.div`
height: 3px;
/* :last-child {
  marginBottom: 0;
} */
position: relative;
background: black;
display: flex;
flex-direction: row;
justify-content: space-around;
z-index: 90;
`;

interface StringLabelProps { isEditable?: boolean, position?: string, isActive: boolean, onClick: () => void };
const StringLabel = styled('span') <StringLabelProps>`
position: ${props => props.position || 'relative'};
text-align: center;
top: -14px;
height: 28px;
width: 28px;
z-index: 99;
line-height: 28px;
font-size: 12px;
${props => props.isEditable && "cursor: pointer;"}
font-weight: bold;
border-radius: 14px;
border: 1px solid;
border-color: ${props => props.theme.colors.black};
color: ${props => props.isActive ? props.theme.colors.white : props.theme.colors.accent};
background-color: ${props => props.isActive ? props.theme.colors.accent : props.theme.colors.white};
`;

type ActiveNotes = Dictionary<{ start: string, end: string, range: string[] }>

type Props = {
  rotated?: boolean;
  scale?: string;
  isEditable?: boolean;
  numberOfFrets: number;
  firstFret?: number;
  activeNotes?: Dictionary<string | null>;
  onChange?: (notes: Dictionary<string | null>) => void;
  onDelete?: () => void;
  name?: string;
}
type State = {
  showInactiveNotes: boolean;
  notes: ActiveNotes;
  activeNotes: Dictionary<string | null>;
}

export type ActiveNoteTransaction = Dictionary<string | null>;

const strings: Dictionary<{ start: string, end: string }> = {
  5: { start: 'E4', end: 'E6' },
  4: { start: 'B2', end: 'B4' },
  3: { start: 'G2', end: 'G4' },
  2: { start: 'D2', end: 'D4' },
  1: { start: 'A2', end: 'A4' },
  0: { start: 'E2', end: 'E4' },
};

const StringCell = (props: {
  isEditable?: boolean,
  position?: string,
  note: string,
  isActive: boolean,
  toggle: () => void,
}) => {
  const [isActive, toggleActive] = React.useState(props.isActive);
  const onClick = props.isEditable ? () => {
    props.toggle();
    toggleActive(!isActive);
  } : () => { };
  return (
    <StringLabel
      isEditable={props.isEditable}
      onClick={onClick}
      isActive={isActive}
      position={props.position}
    >{props.note}</StringLabel>
  );
}

class GuitarNeck extends React.Component<Props, State> {
  static defaultProps = {
    firstFret: 1,
  }
  notes: Dictionary<string[]> = {};
  constructor(props: Props) {
    super(props);
    const notes = {};
    const activeNotes = {};
    const activeNotesFromProps = props.activeNotes || {};
    Object.entries(strings).map(itm => {
      const [key, range] = itm;
      activeNotes[key] = _.get(activeNotesFromProps, key, null);
      notes[key] = {
        ...itm,
        range: chromatic([range.start, range.end]),
      };
    })
    this.state = {
      notes,
      activeNotes,
      showInactiveNotes: true,
    }
  }

  toggleNote = (str: number, note: string) => {
    this.setState(state => {
      const isNoteActive = _.get(state.activeNotes, str) === note;
      return {
        ...state,
        activeNotes: {
          ...state.activeNotes,
          [str]: isNoteActive ? null : note,
        },
      }
    }, () => {
      if (this.props.onChange) {
        this.props.onChange(this.state.activeNotes);
      }
    })
  }

  dots = (index: number) => {
    const { numberOfFrets, firstFret } = this.props
    const fret = index + 1;
    if (
      // this.props.firstFret === 0 &&
      fret === 3 ||
      fret === 5 ||
      fret === 7 ||
      fret === 9
    ) {
      return <div className={css(styles.dot)}></div>;
    }
    if (fret === 0) {
      return (
        <>
          <div className={css(styles.dot)}></div>
          <div className={css(styles.dot)}></div>
        </>
      );
    }
    return null;
  }

  strings = () => {
    const { firstFret = 1, numberOfFrets = 12 } = this.props;
    return (
      <StringContainer>
        {
          Object.entries(this.state.notes).map(data => {
            const [str, notes] = data;
            const sliced = notes.range.slice(firstFret, firstFret + (numberOfFrets));
            return (
              <String key={`string_${str}`}>
                {
                  _.reverse(sliced).map((note, fret) => {
                    const toggle = () => this.toggleNote(+str, note);
                    const isActive =_.get(this.state.activeNotes, +str) === note;
                    return (
                      <StringCell
                        key={`string_${str}_fret_${fret}`}
                        isEditable={this.props.isEditable}
                        note={note}
                        isActive={isActive}
                        toggle={toggle}
                      />
                    )
                  })
                }
              </String>
            )
          })
        }
      </StringContainer>
    )
  }

  frets = () => {
    return (
      <div className={css(styles.fretsOverlay)}>
        {
          _.times(this.props.numberOfFrets, fret => {
            return (
              <div key={`fret_${fret}`} className={css(
                styles.fret,
                this.props.firstFret === 0 && fret === this.props.numberOfFrets - 1 && styles.openStringFret,
              )}>
                <div className={css(styles.dotsContainer)}>
                  {this.dots(fret)}
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }

  openStrings = () => {
    return (
      <NutContainer>
        {
          Object.entries(this.state.notes).map(data => {
            const [str, notes] = data;
            const note = notes.range[0];
            const toggle = () => this.toggleNote(+str, note);
            const isActive = _.includes(_.get(this.state.activeNotes, str, []), note);
            return (
              <String key={`string_${str}_fret_open`}>
                <StringCell
                  note={note}
                  isActive={isActive}
                  toggle={toggle}
                />
              </String>
            )
          })
        }
      </NutContainer>
    )
  }

  render() {
    return (
      <Container rotated={this.props.rotated} scale={this.props.scale}>
        {this.strings()}
        {this.frets()}
        {this.openStrings()}
      </Container>
    );
  }
}
export default GuitarNeck;
