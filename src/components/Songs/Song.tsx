import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { SongInterface } from '../../models';
import { SongActions } from '../../redux/actions/songs';
import Heading from '../common/Heading';

const ListElement = styled.li`
  background-color: white;
  border-bottom: 1px solid;
  border-color: black;
  list-style: none;
`;
const SongDetails = styled.li`
h1, h2, h3,h4,h5 {
  cursor: pointer;
}
`;
const DeleteSongButton = styled.button`
  color: red;
`;
interface Props {
  song: SongInterface;
  deleteSong: (song: SongInterface) => void;
  history: any;
}

interface State {
  showDetails: boolean;
}


class Song extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      showDetails: false,
    }
  }

  deleteSong = () => {
    this.props.deleteSong(this.props.song);
  }

  goToEditor = () => {
    this.props.history.push({
      pathname: '/song-editor',
      state: { song: this.props.song }
    });
  }

  showDetails = () => {
    this.setState(prevState => ({
      showDetails: !prevState.showDetails
    }));
  }

  render() {
    return (
      <ListElement>
        <SongDetails onClick={this.showDetails}>
          <Heading as="h3" onClick={this.showDetails}>
            {this.props.song.name} <DeleteSongButton onClick={this.deleteSong}>Delete Song</DeleteSongButton>
          </Heading>
        </SongDetails>
      </ListElement>
    );
  }
}

export default Song;