import { StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
    container: {
        height: 70,
        width : 80,
    },
    sectionList: {
        width: '80vw',
        height: '20vh',
    },
    progressionList: {},
});

export default styles;