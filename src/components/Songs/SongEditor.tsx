import * as React from 'react';
import { SongInterface } from '../../models';;
import SectionsList from '../Sections/SectionsList';
import { css } from 'aphrodite';
import ProgressionsList from '../Progressions/ProgressionList';
import styles from './styles';

interface Props {
  song: SongInterface;
  location: any;
}


class SongEditor extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <div className={css(styles.container)}>
        <div className={css(styles.sectionList)}>
          <SectionsList />
        </div>
        <div>
          <ProgressionsList />
        </div>
      </div>
    );
  }
}

export default SongEditor;