import * as React from 'react';
import { fas } from '@fortawesome/free-solid-svg-icons'
import styled from '../../../helpers/styled';
import themeColor from '../../../helpers/themeColor';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export interface BaseButtonProps {
  onClick?: () => void;
  bold?: boolean;
  color?: string;
  fontSize?: string | number;
  textColor?: string;
  textAlign?: string;
  padding?: string | number;
  borderRadius?: string | number;
  fontFamily?: string;
  margin?: string | number;
  flex?: boolean;
};

const Icon = styled(FontAwesomeIcon)``;

export const BaseButton = styled("button")<BaseButtonProps>`
${props => props.flex && 'flex: 1;'}
font-weight: ${props => props.bold ? '600' : '300'};
font-size: ${props => props.fontSize ? props.fontSize : 'inherit'};
text-align: ${props => props.textAlign ? props.textAlign : 'center'};
padding: ${props => props.padding ? props.padding : '6px 16px'};
border-radius: ${props => props.borderRadius ? props.borderRadius : '4px'};
font-family: ${props => props.fontFamily ? props.fontFamily : "'Roboto Condensed', sans-serif"};
${props => {
  if (props.margin) {
    return props.margin;
  }
  return ':not(:only-child) { margin-right: 5px; }';
}}cursor: pointer;
color: ${props => props.textColor ? props.textColor : themeColor('white')(props)};
background-color: ${props => props.color ? props.color : themeColor('primary')(props)};
${Icon} {
  margin: 0 5px;
  font-size: 14px
}
:focus, :active {
  outline: none;
}
`;

interface ButtonProps extends BaseButtonProps {
  primary?: boolean;
  icon?: IconProp;
  iconRight?: boolean;
}


const Button: React.FunctionComponent<ButtonProps> = React.memo(props => {
  const icon = props.icon ? <Icon icon={props.icon} /> : null;
  return (
    <BaseButton {...props}>
      {icon && !props.iconRight && icon}
      {props.children}
      {icon && props.iconRight && icon}
    </BaseButton>
  );
});
export default Button;
