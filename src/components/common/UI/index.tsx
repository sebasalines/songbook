import styled from '../../../helpers/styled';

export interface FieldProps extends React.HTMLProps<HTMLInputElement> {
  label?: string;
  onChangeText?: (text: string, e?: React.ChangeEvent<HTMLInputElement>) => void;
}

export const Actions = styled.div`
display: flex;
flex-direction: ${(props: { row?: boolean }) => props.row ? 'row' : 'column'};
justify-content: center;
align-items: center;
`;

// Buttons

export const AddButton = styled.button`
position: absolute;
top: 0;
right: 20px;
height: 24px;
min-width: 24px;
border-radius: 12px;
`;

export const PrimaryButton = styled.button`
padding: 3px 8px;
color: ${props => props.theme.colors.white};
background: ${props => props.theme.colors.primary};
font-size: 14px;
border-color: transparent;
border-radius: 4px;
cursor: pointer;
:active, :hover {
  background: ${props => props.theme.colors.secondary};
}
`;

export const DangerPrimaryButton = styled.button`
padding: 3px 8px;
color: ${props => props.theme.colors.white};
background: ${props => props.theme.colors.accent};
font-size: 14px;
border-color: transparent;
border-radius: 4px;
cursor: pointer;
:active, :hover {
  background: ${props => props.theme.colors.secondary};
}
`;

export const Heading = styled.h1`
font-size: 18px;
font-weight: bold;
color: ${props => props.theme.colors.primary};
`;

export const Page = styled("div") <{ flex?: boolean }>`
display: ${props => props.flex ? 'flex' : 'block'};
padding: 15px 20px;
background: ${props => props.theme.colors.page};
`;

export const DeleteButton = styled.button`
position: absolute;
right: -10px;
top: -10px;
height: 20px;
width: 20px;
border-radius: 10px;
color: whitesmoke;
background: ${props => props.theme.colors.secondary};
`;

export const LightGreenButton = styled.button`
background-color: white;
border: none;
color: green;
padding: 10px;
-webkit-box-shadow: 0px 0px 5px 0px rgba(181,181,181,1);
-moz-box-shadow: 0px 0px 5px 0px rgba(181,181,181,1);
box-shadow: 0px 0px 5px 0px rgba(181,181,181,1);
border-radius: 51px 51px 51px 51px;
-moz-border-radius: 51px 51px 51px 51px;
-webkit-border-radius: 51px 51px 51px 51px;
border: 0px solid #000000;
`;

export const LightRedButton = styled.button`
background-color: white;
border: none;
color: red;
padding: 10px;
-webkit-box-shadow: 0px 0px 5px 0px rgba(181,181,181,1);
-moz-box-shadow: 0px 0px 5px 0px rgba(181,181,181,1);
box-shadow: 0px 0px 5px 0px rgba(181,181,181,1);
border-radius: 51px 51px 51px 51px;
-moz-border-radius: 51px 51px 51px 51px;
-webkit-border-radius: 51px 51px 51px 51px;
border: 0px solid #000000;
`;

// Form related components

export const Input = styled('input') <FieldProps>`
padding: 5px 10px;
margin: 4px;
border-radius: 4px;
color: ${props => props.theme.colors.secondary};
font-size: 12px;
font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
border: 1px solid ${props => props.theme.colors.secondary};
::placeholder {
  color: ${props => props.theme.colors.lightBlue};
}
`;

export const Label = styled.label`
font-size: 14px;
line-height: 22px;
font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
color: whitesmoke;
`;

export const Fields = styled.div`
padding: 0;
margin: 0 20px;
`;

// Containers

export const FieldContainer = styled.div`
display: flex;
flex-direction: column;
justify-content: flex-start;
`;

export const Container = styled.div`
  background-color: ${props => props.theme.colors.white};
  flex: 1;
  display: flex;
`;

export const Content = styled.div`
margin: 20px auto;
padding: 20px;
width: 80vw;
position: relative;
`;
// Lists ul li
export const UnorderedList = styled.ul`
flex: 1;
list-style: none;
padding: 0;
min-height: 15vh;
background-color: white;
-webkit-box-shadow: 0px 2px 8px 0px rgba(94,94,94,0.47);
-moz-box-shadow: 0px 2px 8px 0px rgba(94,94,94,0.47);
box-shadow: 0px 2px 8px 0px rgba(94,94,94,0.47);
`;
export const ListElement = styled.li`
height: 15vh;
display: flex;
min-width: 10vw;
align-items: center;
justify-content: center;
padding: 0 10px 0 10px;
background-color: white;
-webkit-box-shadow: 0px 2x 8px 0px rgba(94,94,94,0.47);
-moz-box-shadow: 0px 2px 8px 0px rgba(94,94,94,0.47);
box-shadow: 0px 2px 8px 0px rgba(94,94,94,0.47);

 &:hover {
    background: #e5e5e5;
  }
`;

export const ListElementDetails = styled.div`
padding: 5px;
height: 15vh;
background: #FFFFE5;
display: flex;
justify-content: space-between;
`;

export const HorizontalList = styled.ul`
display: flex;
height: 15vh;
padding: 0;
min-width: 50vw;
min-height: 20 vh;
list-style: none;
background-color: white;
&:hover {
  background: rgba(63, 136, 197, 0.1);
}
-webkit-box-shadow: 0px 2x 8px 0px rgba(94,94,94,0.47);
-moz-box-shadow: 0px 2px 8px 0px rgba(94,94,94,0.47);
box-shadow: 0px 2px 8px 0px rgba(94,94,94,0.47);

`;

// Chord Components

export const ChordsList = styled.div`
display: flex;
flex-wrap: wrap;
flex-grow: 2;
flex-direction: column;
justify-content: center;
`;

export const ChordPreview = styled('div') <{ numberOfFrets: number }>`
margin-right: 50px !important;
max-width: 600px !important;
${props => props.numberOfFrets > 6 ? 'transform: scale(.86);' : ''}
`;

export const ChordCard = styled.div`
border: 1px solid ${props => props.theme.colors.secondary};
margin: 10px 0;
padding: 14px;
border-radius: 5px;
background-color: ${props => props.theme.colors.page};
display: flex;
flex-direction: row;
position: relative;
`;

export const RowContainer = styled.div`
display: flex;
margin: 0px 0px 30px 0px;
height: 25vh;
background-color: white;
flex-direction: row;
justify-content: space-between;
`;

export const ChordName = styled.span`
font-size: 16px;
line-height: 22px;
color: ${props => props.theme.colors.black};
font-weight: bold;
`;
