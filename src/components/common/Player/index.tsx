import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import MidiPlayer from 'midi-sounds-react';
import { AppState } from '../../../models/redux';
import { ActiveNoteInfo } from '../../Chords/ChordEditor';
import { removeFromQueue } from '../../../redux/actions/midi';

export interface PlayerProps {
  playingQueue: any[];
  removeFromQueue: (uuid: string) => void;
}
interface State {

}
class Player extends React.Component<PlayerProps, State> {
  midiPlayer: React.RefObject<MidiPlayer> = React.createRef();

  componentWillUpdate(nextProps: PlayerProps) {
    if (nextProps.playingQueue.length !== this.props.playingQueue.length) {
      const nextItem = [...nextProps.playingQueue].pop();
      if (nextItem) {
        this.handleQueueItem(nextItem);
      }
    }
  }

  get contextTime() {
    return this.midiPlayer.current
      ? this.midiPlayer.current.contextTime()
      : null;
  }
  
  playNote = (midi: number | null) => {
    if (this.midiPlayer.current && midi) {
      this.midiPlayer.current.playChordAt(this.contextTime, 258, [midi], 1);
    }
  }

  strumNotes = (pitches: number[], up = false) => {
    if (this.midiPlayer.current) {
      if (up) {
        this.midiPlayer.current.playStrumUpNow(258, pitches, 2);
      } else {
        this.midiPlayer.current.playStrumDownNow(258, pitches, 2);
      }
    }
  }
  
  handleQueueItem = item => {
    const type: string | null = _.get(item, 'type', null);
    const uuid: string | null = _.get(item, 'uuid', null);
    if (type) {
      switch(type) {
        case 'single': {
          const pitch: number | null = _.get(item, 'pitch', null);
          this.playNote(pitch);
          break;
        }
        case 'strum': {
          const pitches: number[] = _.get(item, 'pitches', []);
          const up = _.get(item, 'up', false);
          this.strumNotes(pitches, up)
          break;
        }
        default: break;
      }
    }
    if (uuid) {
      this.props.removeFromQueue(uuid);
    }
  }
  
  render() {
    return (
      <MidiPlayer
        ref={this.midiPlayer}
        instruments={[258]}
        appElementName="root"
      />
    )
  }
}
const mapStateToProps = (state: AppState) => ({
  playingQueue: state.midi.queue,
});
const mapDispatchToProps = dispatch => ({
  removeFromQueue: (uuid: string) => dispatch(removeFromQueue(uuid)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Player);
