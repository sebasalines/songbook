import * as React from 'react';
import styled from '../../../helpers/styled';
import themeColor from '../../../helpers/themeColor';

export interface BaseInputProps {
  padding?: string | number;
  margin?: string | number;
  horizontal?: boolean;
};
const BaseInput = styled("input")<BaseInputProps>`
padding: ${props => props.padding ? props.padding : '10px 15px'};
font-size: 16px;
margin: ${props => props.margin ? props.margin : '0'};
border-radius: none;
color: ${themeColor('text')};
flex: 1;
font-family: 'Roboto Condensed', sans-serif;
:placeholder {
  color: ${themeColor('page')};
}
${props => {
  if (props.horizontal) {
    return ':not(:only-child) { margin-right: 5px; }';
  }
}}
:focus, :active {
  outline: none;
}
`;

export interface InputProps extends BaseInputProps, React.HTMLProps<HTMLInputElement> {
  onChangeText?: (text: string) => void;
}
const Input: React.FunctionComponent<InputProps> = props => {
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (props.onChangeText) {
      props.onChangeText(e.currentTarget.value);
    }
    if (props.onChange) {
      props.onChange(e);
    }
  };
  return (
    <BaseInput {...props as any} onChange={onChange} />
  );
};
export default Input;