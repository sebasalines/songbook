import * as React from 'react';
import styled from '../../../helpers/styled';
import themeColor from '../../../helpers/themeColor';

const H1 = styled("h1") <HeadingProps>`
font-size: ${props => props.fontSize ? props.fontSize : '32px;'};
color: ${props => props.color ? props.color : themeColor('text')(props)};
line-height: ${props => {
  return props.fontSize ? `${Number(props.fontSize) * 1.5}px` : '49px';
}};
border-bottom: 4px solid ${themeColor('secondary')};
`;
const H2 = styled("h2") <HeadingProps>`
font-size: ${props => props.fontSize ? props.fontSize : '24px;'};
color: ${props => props.color ? props.color : themeColor('text')(props)};
line-height: ${props => {
  return props.fontSize ? Number(props.fontSize) * 1.5 : '36px';
}};
border-bottom: 4px solid ${props => props.accentColor ? props.accentColor : themeColor('secondary')(props)};
`;
const H3 = styled("h3") <HeadingProps>`
font-size: ${props => props.fontSize ? props.fontSize : '20px;'};
color: ${props => props.color ? props.color : themeColor('text')(props)};
line-height: ${props => {
  return props.fontSize ? Number(props.fontSize) * 1.5 : '30px';
}};
border-bottom: 4px solid ${props => props.accentColor ? props.accentColor : themeColor('secondary')(props)};
`;
const H4 = styled("h4") <HeadingProps>`
font-size: ${props => props.fontSize ? props.fontSize : '18px;'};
color: ${props => props.color ? props.color : themeColor('text')(props)};
line-height: ${props => {
  return props.fontSize ? Number(props.fontSize) * 1.5 : '27px';
}};
border-bottom: 4px solid ${props => props.accentColor ? props.accentColor : themeColor('secondary')(props)};
`;
const H5 = styled("h5") <HeadingProps>`
font-size: ${props => props.fontSize ? props.fontSize : '16px;'};
color: ${props => props.color ? props.color : themeColor('text')(props)};
line-height: ${props => {
  return props.fontSize ? Number(props.fontSize) * 1.5 : '24px';
}};
border-bottom: 4px solid ${props => props.accentColor ? props.accentColor : themeColor('secondary')(props)};
`;

const HeadingComponents = {
  h1: H1,
  h2: H2,
  h3: H3,
  h4: H4,
  h5: H5,
};

interface HeadingProps extends React.HTMLProps<HTMLHeadingElement> {
  as?: "h1" | "h2" | "h3" | "h4" | "h5";
  color?: string;
  fontSize?: string | number;
  accentColor?: string;
}

const Heading: React.FunctionComponent<HeadingProps> = React.memo(props => {
  const { as = 'h1', ...otherProps } = props;
  const Component = HeadingComponents[as];
  return <Component {...otherProps as any} />
});
export default Heading;
