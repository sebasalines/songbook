import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import uuid from 'uuid/v4';
import styled from '../../../helpers/styled';
import { SongChord } from '../../../models';
import { getNeck, dotsForFret, NeckOptions, chordFretInfo } from '../../../helpers/guitarNeckHelpers';
import { notes } from 'tonal-scale';
import Heading from '../../common/Heading';
import { GuitarNeck } from '../ChordEditor/GuitarNeck';
import Button, { BaseButton } from '../../common/Button';
import { strumNotes } from '../../../redux/actions/midi';
import { deleteChord } from '../../../redux/actions/chords';

const Container = styled.div`
display: flex;
flex-direction: column;
align-items: stretch;
justify-content: center;
h5 {
  text-align: center;
}
`;

const NeckContainer = styled.div`
background: ${props => props.theme.colors.white};
display: flex;
position: relative;
align-items: stretch;
flex-direction: row;
justify-content: space-between;
flex: 1;
height: 95px;
min-width: 160px;
max-width: 350px;
border-left: 4px solid ${props => props.theme.colors.black};
border-right: 4px solid ${props => props.theme.colors.black};
z-index: 10;
`;

const Fret = styled.div`
flex: 1;
display: flex;
align-items: stretch;
flex-direction: column;
justify-content: space-between;
position: relative;
border-right: 3px solid ${props => props.theme.colors.black};
:last-child {
  border-right: none;
}
`;

const String = styled.div`
position: relative;
height: 3px;
display: flex;
flex-direction: row;
justify-content: space-around;
z-index: 90;
background-color: ${props => props.theme.colors.black};
`;

const Dot = styled.div`
width: 6px;
height: 6px;
border-radius: 3px;
:not(:only-child) {
  margin-bottom: 4px;
}
background: ${props => props.theme.colors.black};
`;

const DotContainer = styled.div`
position: absolute;
z-index: 99;
top: 5px;
left: 5px;
`;

const ActiveNote = styled.div`
position: relative;
top: -5px;
width: 10px;
height: 10px;
z-index: 99;
border-radius: 5px;
border: 1px solid ${props => props.theme.colors.black};
background: ${props => props.theme.colors.primary};
`;

const Bookmark = styled.div`
position: absolute;
bottom: -30px;
padding: 0 10px;
height: 30px;
background: ${props => props.theme.colors.page};
`;

const BookMarkCaret = styled.div`
height: 0;
width: 0;
left: calc(50% - 16px);
position: absolute;
bottom: -8px;
border-left: 16px solid transparent;
border-right: 16px solid transparent;
border-top: 15px solid ${props => props.theme.colors.page};
`;

const Header = styled.div`
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;
h5 {
  border-bottom: none;
}
.actions {
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
}
${BaseButton} {
  width: 18px;
  height: 18px;
  border-radius: 9px;
  .fa {
    font-size: 14px;
  }
}
`;

export const renderDots = (fret) => _.times(dotsForFret(fret), () => <Dot />);

interface State {
  showName: boolean;
}
export interface ChordPreviewProps {
  play: (notes, up) => void;
  delete: (chord) => void;
  chord: SongChord;
  neckOptions?: NeckOptions;
  showName?: boolean;
  previewOnHover?: boolean;
}
class ChordPreview extends React.Component<ChordPreviewProps, State> {
  uuid: string;
  constructor(props) {
    super(props);
    this.uuid = uuid();
    this.state = {
      showName: false,
    };
  }

  play = () => {
    this.props.play(this.props.chord.activeNotes, false);
  }

  delete = () => this.props.delete(this.props.chord);
  
  render() {
    const { chord, showName, neckOptions = {} } = this.props;
    const neck = getNeck({
      ...neckOptions,
      ...chordFretInfo(chord.activeNotes),
    });
    return (
      <Container>
        {
          showName &&
          <Header>
            <Heading as="h5">{chord.name}</Heading>
            <Button icon="play" onClick={this.play} />
            <Button icon="times" onClick={this.delete} />
          </Header>
        }
        <GuitarNeck
          compact
          mode="flat"
          neck={neck}
          activeNotes={chord.activeNotes}
        />
      </Container>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  delete: (chord: SongChord) => dispatch(deleteChord(chord)),
  play: (notes, up) => dispatch(strumNotes(notes, up)),
})
export default connect(null, mapDispatchToProps)(ChordPreview);
