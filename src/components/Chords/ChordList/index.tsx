import * as React from 'react';
import { connect } from 'react-redux';
import styled from '../../../helpers/styled';
import ChordPreview from '../ChordPreview';
import { SongChord } from '../../../models';

export interface ChordListProps {
  chords: SongChord[];
}

const List = styled.div`
flex: 1;
display: flex;
flex-direction: row;
justify-content: center;
padding: 40px 0;
`;

const ListItem = styled(ChordPreview)`
margin: 10px 20px !important;
`;

const ChordList: React.FunctionComponent<ChordListProps> = props => {
  return (
    <List>
      {
        props.chords.map(chordProps => (
          <ListItem previewOnHover chord={chordProps} />
        ))
      }
    </List>
  );
}

const mapStateToProps = state => ({
  chords: state.chordReducer.chords,
});
export default connect(mapStateToProps)(ChordList);