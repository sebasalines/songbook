import * as React from 'react';
import * as _ from 'lodash';
import * as Detect from 'tonal-detect';
import uuid from 'uuid/v4';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MIDISounds from 'midi-sounds-react';

import { getNeck, StringInfo, PitchInfo, FretInfo } from '../../../helpers/guitarNeckHelpers';
import styled from '../../../helpers/styled';
import ChordPreview, { renderDots, ChordPreviewProps } from '../ChordPreview';
import themeColor from '../../../helpers/themeColor';
import Heading from '../../common/Heading';
import Button from '../../common/Button';
import { SongChord } from '../../../models';
import { createChord, deleteChord } from '../../../redux/actions/chords';
import { GuitarNeck } from './GuitarNeck';
import { connect } from 'react-redux';
import { playNote, strumNotes } from '../../../redux/actions/midi';

const Container = styled.div`
flex: 1;
display: flex;
flex-direction: column;
align-items: stretch;
justify-content: stretch;
`;

const NeckDisplay = styled.div`
flex: 1;
display: flex;
flex-direction: row;
`;

const ControlPanel = styled.div`
display: flex;
flex-direction: row;
padding: 20px;
max-width: 240px;
flex-wrap: wrap;
button {
  flex: 0 0 40%;
  margin: 5px;
}
`;

const StyledChordPreview = styled(ChordPreview)``;

const ChordList = styled.div`
display: flex;
flex-direction: row;
flex-wrap: wrap;
${StyledChordPreview} {
  flex: 1 0 20%
}
`;

interface ChordEditorProps {
  chords: SongChord[];
  playNote: (note: number | null) => void;
  strumChord: (notes: ActiveNoteInfo[], up: boolean) => void;
  addChord: (chord: SongChord) => void;
  deleteChord: (chord: SongChord) => void;
}

export interface ActiveNoteInfo {
  note: FretInfo | null;
  string: number;
}

interface State {
  name: string;
  mode: 'sharp' | 'flat';
  usedFrets: number[];
  useSharps: boolean;
  detectedName: string | null;
  activeNotes: ActiveNoteInfo[];
  chords: SongChord[];
  mirror: {
    x: boolean;
    y: boolean;
  };
}

class ChordEditor extends React.Component<ChordEditorProps, State> {
  midi: React.RefObject<MIDISounds> = React.createRef()
  neck: StringInfo[];
  constructor(props: ChordEditorProps) {
    super(props);
    const mirror = { x: true, y: true };
    this.neck = getNeck({ mirror });
    this.state = {
      mirror,
      name: '',
      mode: 'flat',
      chords: [],
      usedFrets: [],
      detectedName: '',
      useSharps: false,
      activeNotes: this.neck.map(str => ({ string: str.string, note: null })),
    };
  }

  get detectedNames() {
    const { activeNotes, mode } = this.state;
    const pitches = _.compact(activeNotes.reduce((pitches: Array<string | undefined>, note) => {
      if (note.note) {
        return [...pitches, note.note.pitch[mode]];
      }
      return pitches;
    }, []));
    return Detect.chord(pitches);
  }

  openStringNote = (stringNumber: number) => {
    const string = this.neck.find(itm => itm.string === stringNumber);
    if (!string) {
      return null;
    }
    const openFret = string.frets.find(itm => itm.fret === 0);
    return openFret || null;
  }

  addChord = () => {
    const activeNotes = [...this.state.activeNotes];
    const chord: SongChord = {
      id: uuid(),
      name: this.detectedNames[0] || 'Chord X',
      activeNotes,
    };
    this.props.addChord(chord)
  }

  toggleSharps = () => this.setState(state => {
    const mode = state.mode === 'flat'
      ? 'sharp' : 'flat';
    return { mode };
  })

  toggleNote = (data: { fret: number, string: number }) => () => {
    this.setState(state => {
      const string = this.neck.find(itm => itm.string === data.string);
      if (!string) {
        return { ...state };
      };
      const newNote = string.frets.find(itm => itm.fret === data.fret) || null;
      const activeNotes = [...state.activeNotes].map(itm => {
        if (itm.string === data.string) {
          const note = itm.note && itm.note.fret === data.fret
            ? null : newNote;
          if (note) {
            this.props.playNote(note.pitch.midi);
          }
          return {
            ...itm,
            note,
          };
        }
        return itm;
      });
      return {
        activeNotes,
      };
    })
  }

  toggleMirror = (direction: 'x' | 'y') => () => this.setState(state => {
    const mirror = {
      ...state.mirror,
      [direction]: !state.mirror[direction],
    };
    this.neck = getNeck({ mirror });
    return { mirror };
  });

  chromatic = (up = false) => () => this.setState(state => {
    const activeNotes = [...state.activeNotes].map(str => {
      if (str.note) {
        const string = this.neck.find(itm => itm.string === str.string);
        const fretCompare = up
          ? str.note.fret + 1
          : str.note.fret - 1;
        let note = string
          ? string.frets.find(itm => itm.fret === fretCompare) || null
          : null;
        if (
          up && str.note.fret === 12 || !up && str.note.fret === 0
        ) {
          note = str.note;
        }
        return {
          ...str,
          note,
        };
      }
      return str;
    });
    return { activeNotes };
  });

  purgeChords = () => {
    this.props.chords.forEach(itm => {
      this.props.deleteChord(itm);
    });
  }

  strumCurrentNotes = (up = false) => () => this.props.strumChord(this.state.activeNotes, up);

  render() {
    return (
      <Container>
        <NeckDisplay>
          <GuitarNeck
            toggleNote={this.toggleNote}
            mode={this.state.mode}
            neck={this.neck}
            mirror={{ x: true, y: true }}
            activeNotes={this.state.activeNotes}
          />
          <ControlPanel>
            <Button onClick={this.toggleSharps}># => b</Button>
            <Button onClick={this.addChord} icon="plus">Add</Button>
            <Button onClick={this.toggleMirror('x')} icon="ruler-horizontal"></Button>
            <Button onClick={this.toggleMirror('y')} icon="ruler-vertical"></Button>
            <Button onClick={this.strumCurrentNotes()} icon="arrow-down">Strum Down</Button>
            <Button onClick={this.strumCurrentNotes(true)} icon="arrow-up">Strum Up</Button>
            <Button onClick={this.chromatic(!this.state.mirror.x)} icon="chevron-left" />
            <Button onClick={this.chromatic(this.state.mirror.x)} icon="chevron-right" />
          </ControlPanel>
        </NeckDisplay>
        <Heading as="h5">{this.detectedNames.join(', ')}</Heading>
        <ChordList>
          {this.props.chords.map(chord => (
            <StyledChordPreview
              showName
              key={chord.id}
              chord={chord}
              neckOptions={{ mirror: { ...this.state.mirror } }}
            />
          ))}
        </ChordList>
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  chords: state.chordReducer.chords,
});
const mapDispatchToProps = dispatch => ({
  playNote: (note: number | null) => dispatch(playNote(note)),
  strumChord: (notes: ActiveNoteInfo[], up: boolean) => dispatch(strumNotes(notes, up)),
  addChord: (chord: SongChord) => dispatch(createChord(chord)),
  deleteChord: (chord: SongChord) => dispatch(deleteChord(chord)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ChordEditor);