import * as React from 'react';
import * as _ from 'lodash';
import styled from '../../../helpers/styled';
import themeColor from '../../../helpers/themeColor';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ActiveNoteInfo } from '.';
import { renderDots } from '../ChordPreview';
import { StringInfo, PitchInfo } from '../../../helpers/guitarNeckHelpers';
import colors from '../../../constants/colors';


const Note = styled('span')<{ compact?: boolean, isMuted?: boolean, isActive: boolean }>`
font-size: 12px;
line-height: 18px;
font-weight: bold;
color: ${props => props.isActive ? themeColor('white')(props) : themeColor('transparent')(props)};
background: ${props => props.isActive || props.isMuted ? themeColor(props.compact ? 'accent' : 'primary')(props) : themeColor('transparent')(props)};
border: ${props => props.compact ? 0 : 3}px solid ${props => props.isActive || props.isMuted ? themeColor('primary')(props) : themeColor('transparent')(props)};
border-radius: ${props => props.isMuted ? '12px' : '4px'};
position: absolute;
top: -9px;
z-index: 99;
display: block;
text-align: center;
overflow: hidden;
cursor: pointer;
${props => {
  if (props.compact) {
    return `
height: 10px !important;
width: 10px !important;
line-height: 0;
top: -3px;
`;
  }
  return props.isMuted ? `
height: 18px;
width: 18px;
` : `
padding: 0 5px;
min-width: 24px;
`;
}}
`;


const NeckContainer = styled('div')<{ compact?: boolean, numberOfStrings: number }>`
flex: 1;
background: ${themeColor('white')};
display: flex;
position: relative;
align-items: stretch;
flex-direction: row;
justify-content: space-between;
height: ${props => props.numberOfStrings * (props.compact ? 10 : 33.3)}px;
min-width: ${props => props.compact ? 150 : 700}px;
max-width: ${props => props.compact ? 258 : 1200}px;
margin: ${props => props.compact ? 8 : 30}px;
border-left: ${props => props.compact ? 2 : 4}px solid ${themeColor('black')};
border-right: ${props => props.compact ? 2 : 4}px solid ${themeColor('black')};
z-index: 10;
`;

const Fret = styled.div`
flex: 1;
display: flex;
align-items: stretch;
flex-direction: column;
justify-content: space-between;
position: relative;
border-right: 3px solid ${themeColor('black')};
:last-child {
  border-right: none;
}
`;

const Nut = styled.div`
width: 5px;
display: flex;
position: relative;
align-items: stretch;
flex-direction: column;
justify-content: space-between;
background: ${themeColor('black')};
`;

const String = styled.div`
position: relative;
height: 3px;
display: flex;
flex-direction: row;
justify-content: space-around;
z-index: 90;
background-color: ${themeColor('black')};
`;


const DotContainer = styled.div`
position: absolute;
z-index: 99;
top: 5px;
left: 5px;
`;

interface StringsForFretProps {
  isNut?: boolean;
  compact?: boolean;
  mode: 'flat' | 'sharp';
  fret: number;
  strings: { string: number; pitch: PitchInfo | null }[];
  activeNotes?: ActiveNoteInfo[];
  onClick?: (data: { fret: number, string: number }) => () => void;
}
export const StringsForFret: React.FunctionComponent<StringsForFretProps> = props => {
  const notes: React.ReactNode[] = [];
  return (
    <>
      {
        props.strings.map(string => {
          const activeNoteOnString = [...props.activeNotes || []].find(itm => (itm.string === string.string));
          const isActive = _.get(activeNoteOnString, 'note.fret', null) === props.fret;
          const isMutedOpenString = Boolean(props.fret === 0 && _.get(activeNoteOnString, 'note') === null);
          const note = _.get(string, `pitch[${props.mode}]`, '');
          const value = isMutedOpenString ? <FontAwesomeIcon icon="times" /> : note;
          return (
            <String key={`strings_${string.string}_fret_${props.fret}`}>
              <Note
                compact={props.compact}
                onClick={
                  props.onClick
                    ? props.onClick({ string: string.string, fret: props.fret })
                    : (() => { })
                }
                isMuted={isMutedOpenString}
                isActive={isMutedOpenString || isActive}
              >
                {
                  !props.compact ? value : null
                }
              </Note>
            </String>
          );
        })
      }
    </>
  );
}

interface GuitarNeckProps {
  mode: 'flat' | 'sharp';
  compact?: boolean;
  neck: StringInfo[];
  maxFrets?: number;
  toggleNote?: StringsForFretProps["onClick"];
  activeNotes?: any;
  mirror?: { x?: boolean; y?: boolean };
}
const dataMapper = (fret: number, neck: StringInfo[]) => neck.map(str => {
  const fretInfo = str.frets.find(itm => itm.fret === fret);
  const pitch: PitchInfo | null = _.get(fretInfo, 'pitch', null);
  return {
    string: str.string,
    pitch,
  };
});

export const GuitarNeck: React.FunctionComponent<GuitarNeckProps> = React.memo(props => {
  const { compact = false, mode, toggleNote: onClick, activeNotes } = props;
  const neck = props.neck;
  const availableFrets = _.get(neck, '[0].frets', []);
  const frets = availableFrets.map((itm) => {
    const strings = dataMapper(itm.fret, neck);
    const stringProps = {
      mode,
      compact,
      strings,
      onClick,
      activeNotes,
      fret: itm.fret,
    };
    return itm.fret === 0
      ? (
        <Nut>
          <StringsForFret {...stringProps} />
        </Nut>
      ) : (
        <Fret key={`fret_container_${itm.fret}`}>
          <DotContainer>{renderDots(itm.fret)}</DotContainer>
          <StringsForFret {...stringProps} />
        </Fret>
      );
  });
  return (
    <NeckContainer compact={compact} numberOfStrings={neck.length}>
      {frets}
    </NeckContainer>
  );
});